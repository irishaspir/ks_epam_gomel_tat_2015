package com.epam.gomel.tat.ispiridonova.bo.error;

/**
 * Created by Ira on 26.07.2015.
 */
public class MessageMistake {

    private String message;

    public MessageMistake(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

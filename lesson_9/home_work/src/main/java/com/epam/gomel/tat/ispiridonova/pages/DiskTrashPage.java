package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

public class DiskTrashPage {

    private static final By TO_FILES_LOCATOR = By.xpath("//div[contains(@class,' ns-view-visible')and contains(@data-key,'Current=trash')]//a[@href='/client/disk' and contains(@class,'item-nav')]");
    private static final String RESTORE_FILE_BUTTON_LOCATOR_PATTERN = "//button[contains(@data-click-action, 'resource.restore') and contains(@data-params,'%s')]";
    private static final String FILE_IN_TRASH_LOCATOR_PATTERN = "//div[contains(@data-key, 'display=normal')]//div[contains(@title,'%s') and not(contains(@class,'nb-resource_document'))]";


    public DiskTrashPage chooseFileAndRestore(String fileName) {
        Logger.log.trace("Choose file ");
        Browser.get().waitElementIsPresent(By.xpath(String.format(FILE_IN_TRASH_LOCATOR_PATTERN, fileName)));
        Browser.get().click(By.xpath(String.format(FILE_IN_TRASH_LOCATOR_PATTERN, fileName)));
        Logger.log.trace("Restore file");
        Browser.get().click(By.xpath(String.format(RESTORE_FILE_BUTTON_LOCATOR_PATTERN, fileName)));
        return new DiskTrashPage();
    }

    public DiskPage goToFiles() {
        Logger.log.trace("Go to  Disk " + TO_FILES_LOCATOR);
        Browser.get().click(TO_FILES_LOCATOR);
        return new DiskPage();
    }
}

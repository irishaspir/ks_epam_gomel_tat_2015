package com.epam.gomel.tat.ispiridonova.tests;

import com.epam.gomel.tat.ispiridonova.pages.ErrorLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TestCaseUnsuccessLoginToYanex {

    private String userLogin = "test-box-IS";
    private String userPassword = "Qwert12";
    private final String MESSAGE_MISTAKE = "Неправильный логин или пароль.";

    @Test(description = "Try login and check Message Error")
    public void login() {
        new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
        new ErrorLoginPage()
                .checkMessageError(MESSAGE_MISTAKE);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}






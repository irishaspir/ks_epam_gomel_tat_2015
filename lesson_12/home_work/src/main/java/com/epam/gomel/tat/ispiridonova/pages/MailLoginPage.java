package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import org.openqa.selenium.By;

public class MailLoginPage {

    private static final By LOGIN_INPUT_LOCATOR = By.name("login");
    private static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    private static final By BUTTON_ENTER_LOCATOR = By.xpath("//div[@class='new-left']//button[@type='submit']");
    private static final By ERROR_MESSAGE_LOCATOR = By.xpath("//div[contains(@class, 'js-messages')]//div[contains(@class, 'error-msg')]");
    private static final String BASE_URL = GlobalConfig.getBASEURL();

    public MailLoginPage open() {
        Browser.get().open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        Browser.get().type(LOGIN_INPUT_LOCATOR, login);
        Browser.get().type(PASSWORD_INPUT_LOCATOR, password);
        Browser.get().submit(BUTTON_ENTER_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public String errorMessage() {
        return Browser.get().getText(ERROR_MESSAGE_LOCATOR);
    }
}

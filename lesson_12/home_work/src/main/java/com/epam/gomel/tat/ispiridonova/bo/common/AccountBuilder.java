package com.epam.gomel.tat.ispiridonova.bo.common;

/**
 * Created by ispiridonova on 10.09.2015.
 */
public class AccountBuilder {

    private static final String EMAIL = "test-box-IS@yandex.ru";
    private static final String login = "test-box-IS";
    private static final String password = "Qwert123";

    public static Account createUser() {
        return new Account();
    }

    public static Builder build(Account user) {
        return new Builder(user);
    }

    public static class Builder {
        Account accountToBuild;

        public Builder(Account user) {
            accountToBuild = user;
        }

        public Builder withCorrectLogin() {
            accountToBuild.setLogin(login);
            return this;
        }

        public Builder withCorrectPassword() {
            accountToBuild.setPassword(password);
            return this;
        }

        public Builder withIncorrectPassword(String password) {
            accountToBuild.setPassword(password);
            return this;
        }

        public Builder withEmail() {
            accountToBuild.setEmail(EMAIL);
            return this;
        }
    }
}

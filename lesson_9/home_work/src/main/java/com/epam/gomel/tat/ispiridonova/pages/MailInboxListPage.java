package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

public class MailInboxListPage {

    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@data-action='tospam']");
    private static final By COMPOSE_LINK_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@data-action='delete']");
    private static final String EMAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";
    private static final String RECEIVED_EMAIL_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-messages-item')]//div[not(.//span[@class='b-messages__folder'])]//a[contains(@href, '#message')]";
    private static final String CHECKBOX_EMAIL_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-messages-item')]//div[not(contains(@class,'open'))]//div[not(.//span[@class='b-messages__folder'])]//input[@class='b-messages__message__checkbox__input']";

    public ComposeMailPage openComposeMailPage() {
       Browser.get().click(COMPOSE_LINK_LOCATOR);
        return new ComposeMailPage();
    }

    public MailInboxListPage openBoxMessage(String mailSubject) {
       Browser.get().waitElementIsPresent(By.xpath(String.format(EMAIL_LINK_LOCATOR_PATTERN,mailSubject)));
       Browser.get().click(By.xpath(String.format(EMAIL_LINK_LOCATOR_PATTERN,mailSubject)));
       Browser.get().waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public ReceiveMessagePage openReceiveMessage(String mailContent) {
       Browser.get().click(By.xpath(String.format(RECEIVED_EMAIL_LOCATOR_PATTERN,mailContent)));
       Browser.get().waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }

    public MailInboxListPage markEmail(String mailContent) {
       Browser.get().click(By.xpath(String.format(CHECKBOX_EMAIL_LOCATOR_PATTERN, mailContent)));
       Browser.get().waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailInboxListPage deleteEmail() {
       Browser.get().click(DELETE_LINK_LOCATOR);
       Browser.get().waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailInboxListPage spamEmail() {
       Browser.get().click(SPAM_LINK_LOCATOR);
       Browser.get().waitForAjaxProcessed();
        return new MailInboxListPage();
    }
}


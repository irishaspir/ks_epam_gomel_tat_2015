package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailInboxListPage extends AbstractBasePage {

    private static final By COMPOSE_LINK_LOCATOR = By.xpath("//a[@href='#compose']");
  //  private static final String RECEIVE_EMAIL_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-messages-item')]//div[not(.//span[@class='b-messages__folder'])]//a[contains(@href, '#message')]";

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_LINK_LOCATOR);
        return new ComposeMailPage();
    }

    public MailInboxListPage openBoxMessage(String mailSubject) {
        browser.waitForElementInboxPresent(mailSubject);
        browser.openBoxEmail(mailSubject);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public ReceiveMessagePage openReceiveMessage(String mailContent) {
        browser.openReceiveEmailInInbox(mailContent);
        browser.waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }

    public MailInboxListPage markEmail(String mailContent) {
        browser.markInboxEmail(mailContent);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailInboxListPage deleteEmail() {
        browser.deleteEmail();
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailInboxListPage spamEmail() {
        browser.spamEmail();
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

  /*  public boolean isMessagePresent(String mailContent) {
        return browser.isPresent(By.xpath(String.format(RECEIVE_EMAIL_LOCATOR_PATTERN, mailContent)));
    }*/
}


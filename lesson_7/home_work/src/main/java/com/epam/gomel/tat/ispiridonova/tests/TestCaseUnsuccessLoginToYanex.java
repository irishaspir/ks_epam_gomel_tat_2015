package com.epam.gomel.tat.ispiridonova.tests;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.error.MessageMistake;
import com.epam.gomel.tat.ispiridonova.bo.error.MessageMistakeBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.service.ErrorGuiService;
import com.epam.gomel.tat.ispiridonova.service.LoginGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseUnsuccessLoginToYanex {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private ErrorGuiService errorLoginPage = new ErrorGuiService();
    private Account unsuccessMail = AccountBuilder.getUnsuccessMail();
  //  private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MessageMistake loginError = MessageMistakeBuilder.getMessageMistake();

    @Test(description = "Try login ")
    public void login() {
        Logger.log.trace("TestCaseUnsuccessLoginToYanex");
        loginGuiService.loginToAccountMailbox(unsuccessMail);
    }

    @Test(description = "check message mistake login ", dependsOnMethods = "login")
    public void checkMessageMistake(){
      Assert.assertTrue(errorLoginPage.isErrorMessage(loginError)," Message mistake is not right");
    }
}






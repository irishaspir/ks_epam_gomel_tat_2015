package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;

public abstract class AbstractBasePage {

    protected Browser browser;

    public AbstractBasePage() {
        this.browser = Browser.get();
    }
}

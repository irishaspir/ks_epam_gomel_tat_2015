package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailSpamListPage extends AbstractBasePage {

    public static final String EMAIL_CHECK_LOCATOR_PATTERN = "//span[@title='%s']";

    public boolean isMessagePresent(String mailSubject) {
        return browser.isPresent(By.xpath(String.format(EMAIL_CHECK_LOCATOR_PATTERN, mailSubject)));
    }

    public MailSpamListPage markEmail(String mailSubject) {
        browser.markSpamEmail(mailSubject);
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public MailSpamListPage notSpamEmail() {
        browser.notSpamEmail();
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public ReceiveMessagePage openEmailInSpam(String mailSubject) {
        browser.openEmailIn(mailSubject);
        browser.waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }
}

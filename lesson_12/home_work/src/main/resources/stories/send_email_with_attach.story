Feature: Send email with attach

Meta:
$Send email with attach

Narrative:
As a Mailbox user
I want to login in mailbox and send email with attach
So that I have access to mailbox listand I see mail in inbox

Scenario: scenario description
Given Actor has login, password and Actor login to mailbox
When Actor send email with attach
Then Actor see email with attach in inbox
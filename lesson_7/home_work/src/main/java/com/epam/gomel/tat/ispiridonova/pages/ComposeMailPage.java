package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class ComposeMailPage extends AbstractBasePage {

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        return sendMail(mailTo, mailSubject, mailContent, null);
    }

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent, String attachFile) {
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, mailSubject);
        browser.type(MAIL_TEXT_LOCATOR, mailContent);
        if (attachFile != null) {

            browser.attachFile(ATTACH_FILE_LOCATOR, attachFile);
        }
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}

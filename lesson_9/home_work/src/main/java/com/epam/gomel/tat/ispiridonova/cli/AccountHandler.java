package com.epam.gomel.tat.ispiridonova.cli;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.spi.Messages;


public class AccountHandler extends OptionHandler<Account> {

    public AccountHandler(CmdLineParser parser, OptionDef option, Setter<? super Account> setter) {
        super(parser, option, setter);
    }

    @Override
    public int parseArguments(Parameters parameters) throws CmdLineException {
        if (this.option.isArgument()) {
            String valueStr = parameters.getParameter(0).toLowerCase();
            if (valueStr == null || valueStr.isEmpty()) {
                throw new CmdLineException(this.owner, Messages.ILLEGAL_CHAR, new String[]{valueStr});
            } else {
                String[] parts = valueStr.split(":");
                if (parts.length < 2)
                    throw new CmdLineException(this.owner, Messages.ILLEGAL_CHAR, new String[]{valueStr});

                Account account = new Account(parts[0], parts[1], parts[2]);
                this.setter.addValue(account);
                return 1;
            }
        } else {
            this.setter.addValue(null);
            return 0;
        }
    }

    @Override
    public String getDefaultMetaVariable() {
        return null;
    }
}

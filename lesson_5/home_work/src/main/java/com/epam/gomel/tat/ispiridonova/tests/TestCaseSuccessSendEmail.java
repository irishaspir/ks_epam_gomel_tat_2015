package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TestCaseSuccessSendEmail {

    private String userLogin = "test-box-IS";
    private String userPassword = "Qwert123";
    private String mailTo = "test-box-IS@yandex.ru";
    private String mailSubject;
    private String mailContent;
    private int fileNameLetter = 10; //
    private int contentLetter = 20;


    @Test(description = "Login")
    public void login() {
        new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
    }

    @Test(description = "Send letter", dependsOnMethods = "login")
    public void sendLetter() {
        RandomUtils generateRandom = new RandomUtils();
        mailSubject = generateRandom.subject(fileNameLetter);
        mailContent = generateRandom.content(contentLetter);
        new MailboxBasePage()
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent);
    }

    @Test(description = "Check Letter", dependsOnMethods = "sendLetter")
    public void checkLetter() {
        boolean checkMessage = new MailboxBasePage()
                .openSendPage()
                .isMessagePresent(mailSubject);
        Assert.assertTrue(checkMessage, "Message is absent");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}

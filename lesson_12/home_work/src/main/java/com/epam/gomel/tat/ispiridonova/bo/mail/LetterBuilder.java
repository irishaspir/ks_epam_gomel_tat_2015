package com.epam.gomel.tat.ispiridonova.bo.mail;

import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import com.epam.gomel.tat.ispiridonova.utils.Utils;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;

public class LetterBuilder {

    private static String receiver = "test-box-IS@yandex.ru";
    private static String subject;
    private static String content;
    private static File attach = null;
    private static int length = GlobalConfig.getLength();

    public static Letter randomValues() {
        subject =  RandomStringUtils.randomAlphabetic(length);
        content =  RandomStringUtils.randomAlphabetic(length);
        Letter letter = new Letter(receiver,subject,content,attach);
        return letter;
    }

    public static Letter randomValuesWithAttach() {
        subject = RandomStringUtils.randomAlphabetic(length);
        content = RandomStringUtils.randomAlphabetic(length);
        attach = new Utils().createFile();
        Letter letter = new Letter(receiver,subject,content,attach);
        return letter;
    }
}


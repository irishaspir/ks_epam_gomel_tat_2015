package com.epam.gomel.tat.ispiridonova.utils;

import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import com.google.common.base.Function;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Utils {

    private String pathAttachFile = GlobalConfig.getAttachDir();
    private String pathDownloadFile = GlobalConfig.getDownloadDirFirefox();
    private String fileType = GlobalConfig.getFileType();
    private int WAIT_FILE_DOWNLOAD_SECONDS = GlobalConfig.getWaitFileDownloadSeconds();
    private int length = GlobalConfig.getLength();
    private File file;
    private boolean resultEqual;
    private static final Logger LOGGER = Logger.getLogger(Utils.class);

    public File createFile() {
        new File(pathAttachFile);
        file = FileUtils.getFile(this.pathAttachFile, fileName(length) + this.fileType);
        try {
            FileUtils.write(file, fileName(length));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public boolean isFilesEqual(File attachFile) {
        try {
            File file1 = new File(this.pathAttachFile + "\\" + attachFile.getName());
            File file2 = new File(attachFile.getAbsolutePath());
            resultEqual = FileUtils.contentEquals(file1, file2);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return resultEqual;
    }

    public void waitForFile(File file) {
        LOGGER.trace("Waiting download file ");
        File fileWait = new File(file.getAbsolutePath());
        FluentWait<File> wait = new FluentWait<File>(fileWait).withTimeout(WAIT_FILE_DOWNLOAD_SECONDS, TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            @Override
            public Boolean apply(File fileWait) {
                com.epam.gomel.tat.ispiridonova.reporting.Logger.trace("Download File - " + fileWait.exists());
                return fileWait.exists();
            }
        });
    }

    public void waitForFileOnDisk(String fileName) {
        File fileWait = new File(this.pathDownloadFile + "\\" + fileName);
        FluentWait<File> wait = new FluentWait<File>(fileWait).withTimeout(WAIT_FILE_DOWNLOAD_SECONDS, TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            @Override
            public Boolean apply(File fileWait) {
                com.epam.gomel.tat.ispiridonova.reporting.Logger.trace("Download File - " + fileWait.exists());
                return fileWait.exists();
            }
        });
    }

    public String fileName(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = Utils.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return null;
    }
}

package com.epam.gomel.tat.ispiridonova.ui;

public enum BrowserType {

    REMOTE("firefox*"),
    FIREFOX("firefox"),
    HTMLUNIT("htmlunit"),
    CHROME("chrome");

    private String type;

    BrowserType(String alias) {
        this.type = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for(BrowserType type: BrowserType.values()){
            if(type.getType().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getType() {
        return type;
    }
}

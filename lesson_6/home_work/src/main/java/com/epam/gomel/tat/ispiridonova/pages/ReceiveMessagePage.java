package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import org.openqa.selenium.By;

public class ReceiveMessagePage extends AbstractBasePage {

    private final String MAIL_SUBJECT_LOCATOR_PATTERN = "//div[contains(@class,'message-subject')]//span[text()='%s']";
    private final String MAIL_CONTENT_LOCATOR_PATTERN = "//div[contains(@class,'block-message-body-box')]//*[contains(text(),'%s')]";
    private final String MAIL_ADRESS_LOCATOR_PATTERN = "//div[contains(@class,'b-message-head__field__content')]//*[contains(text(),'%s')]";
    private final String MAIL_ATTACH_LOCATOR_PATTERN = "//div[@class='b-message-head__field_from__i']//*[contains(text(),'%s')]";


    public boolean checkReceiveMessage(MailLetter letter) {
        browser.isPresent(By.xpath(String.format(MAIL_SUBJECT_LOCATOR_PATTERN, letter.getSubject())));
        browser.isPresent(By.xpath(String.format(MAIL_CONTENT_LOCATOR_PATTERN, letter.getContent())));
        browser.isPresent(By.xpath(String.format(MAIL_ADRESS_LOCATOR_PATTERN, letter.getReceiver())));
        if (letter.getReceiver() != null) {

            browser.isPresent(By.xpath(String.format(MAIL_ATTACH_LOCATOR_PATTERN, letter.getReceiver())));
        }
        return true;
    }

    public void downloadFile(String mailSubject) {
        browser.downloadFile(mailSubject);
    }
}

package com.epam.gomel.tat.ispiridonova.service;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;

public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.log.info("Login to account " + account.getLogin());
        MailboxBasePage mailbox = new MailLoginPage()
                .open()
                .login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null && !userEmail.equals(account.getEmail())) {
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public boolean isLoginSuccess(){
        Logger.log.trace("Check success login");
        return new MailboxBasePage().isCheckLogin();
    }
}

Feature: Delete email

Meta:
$Delete email

Narrative:
As a Mailbox user
I want to login in mailbox, send email and delete email
So that I have access to mailbox list and I see mail in delete list

Scenario: delete email
Given Actor is in mailbox, Actor send email
When Actor delete email
Then Actor see email in delete list
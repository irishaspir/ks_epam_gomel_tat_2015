package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailSendListPage extends AbstractBasePage {

    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMessagePresent(String subject) {
        return browser.isPresent(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
    }
}

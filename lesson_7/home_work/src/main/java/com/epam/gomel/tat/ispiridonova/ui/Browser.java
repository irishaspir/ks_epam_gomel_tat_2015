package com.epam.gomel.tat.ispiridonova.ui;

import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.utils.MyUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Browser {

    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@data-action='delete']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@data-action='tospam']");
    private static final By NOT_SPAM_LINK_LOCATOR = By.xpath("//a[@data-action='notspam']");
    private static final By CHECK_MAIL_LOCATOR = By.xpath("//a[contains(@id,'nb-1')]");
    private static final By LOGOUT = By.xpath("//div[not(.//a[contains(@data-action,'exitAll')])]//a[contains(@href,'logout')]");

    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";
    private static final String RECEIVE_EMAIL_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-messages-item')]//div[not(.//span[@class='b-messages__folder'])]//a[contains(@href, '#message')]";
    private static final String BUTTON_DOWNLOAD_LOCATOR_PATTERN = "//*[contains(text(),'%s')]//ancestor::div[contains(@class, 'b-message-attachments_head')]//a[contains(@href,'message_part')]";
    private static final String MARK_EMAIL_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-thread')]//div[not(.//span[@class='b-messages__folder'])]//input[@class='b-messages__message__checkbox__input']";
    private static final String MARK_SPAM_EMAIL_LOCATOR_PATTERN = "//span[@title='%s']/ancestor::div[contains(@class, 'block-messages-wrap')]//input[@class='b-messages__message__checkbox__input']";
    private static final String OPEN_EMAIL_LOCATOR_PATTERN = "//*[contains(@title,'%s')]/ancestor::a";

    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 40;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 40;
    private static final int AJAX_TIMEOUT = 40;
    private static final int WAIT_ELEMENT_TIMEOUT = 40;
    private static final int TIME_OUT_IN_SECONDS = 40;

    private WebDriver driver;

    private static Map<Thread, Browser> instances = new HashMap<>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = GlobalConfig.getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                new File(FileUtils.getTempDirectoryPath() + "\\DownloadDir").mkdir();
                GlobalConfig.setDownloadDir(FileUtils.getTempDirectoryPath() + "\\DownloadDir");
                Logger.log.trace("Open Firefox: pageLoadTimeout " + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS + " host: " + GlobalConfig.getLocalHost() + " port: " + GlobalConfig.getPort() );
                try {
                    System.out.println("http://" + GlobalConfig.getLocalHost() + ":" + GlobalConfig.getPort() + "/wd/hub");
                    driver = new RemoteWebDriver(new URL("http://" + GlobalConfig.getLocalHost() + ":" + GlobalConfig.getPort() + "/wd/hub"),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                //   driver.manage().window().maximize();
                break;
            case FIREFOX:
                new File(FileUtils.getTempDirectoryPath() + "\\DownloadDir").mkdir();
                GlobalConfig.setDownloadDir(FileUtils.getTempDirectoryPath() + "\\DownloadDir");
                Logger.log.trace("Open Firefox: pageLoadTimeout " + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS);
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                // driver.manage().window().maximize();
                break;
            case CHROME:
                new File(FileUtils.getUserDirectoryPath() + "\\Downloads\\DownloadDir").mkdir();
                GlobalConfig.setDownloadDir(FileUtils.getUserDirectoryPath() + "\\Downloads\\DownloadDir");
                System.setProperty("webdriver.chrome.driver", "target\\suites-xml\\chromedriver.exe");
                Logger.log.trace("Open Chrome: pageLoadTimeout " + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS);
                driver = new ChromeDriver(getChromeOption());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                //  driver.manage().window().maximize();
                break;
        }
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        Logger.log.trace("Download dir " + GlobalConfig.getDownloadDir() + " Type files to save " + FIREFOX_MIME_TYPES_TO_SAVE);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalConfig.getDownloadDir());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    private static DesiredCapabilities getChromeOption() {
        String downloadFilepath = FileUtils.getUserDirectoryPath() + "\\Downloads\\DownloadDir";
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", downloadFilepath);
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--test-type");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        return cap;
    }


    public void open(String url) {
        Logger.log.trace("Open url " + url);
        driver.get(url);
    }

    public void killBrowser() {
        Logger.log.trace("Close Browser");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.close();
                try {
                    Thread.sleep(5000);
                    instance.driver.quit();
                } catch (Exception e) {
                }
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void click(By locator) {
        Logger.log.trace("Click on element " + locator);
        driver.findElement(locator).click();
    }

    public void type(By locator, String text) {
        Logger.log.trace("Type text " + text + " in " + locator);
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(text);
    }

    public void attachFile(By locator, String text) {
        Logger.log.trace("Type to " + locator + "  path of file " + text);
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isPresent(By locator) {
        Logger.log.trace("Find element " + locator);
        return driver.findElements(locator).size() > 0;
    }

    public String errorMessage(By locator) {
        Logger.log.trace("Error message " + locator);
        return driver.findElement(locator).getText();
    }

    public void waitForElementInboxPresent(final String locator) {
        Logger.log.trace("Wait " + TIME_OUT_IN_SECONDS + " element " + locator + " is present in Inbox");
        new WebDriverWait(driver, TIME_OUT_IN_SECONDS).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, locator)));
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.log.trace("Wait " + WAIT_ELEMENT_TIMEOUT + " element " + locator + " is visible");
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void submit(By locator) {
        Logger.log.trace("Click on " + locator);
        driver.findElement(locator).submit();
    }

    public void openBoxEmail(String mailSubject) {
        Logger.log.trace("Open box " + mailSubject + " in " + String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject));
        driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))).click();
    }

    public void openReceiveEmailInInbox(String mailContent) {
        Logger.log.trace("Open email in Inbox" + mailContent + " in " + String.format(RECEIVE_EMAIL_LOCATOR_PATTERN, mailContent));
        driver.findElement(By.xpath(String.format(RECEIVE_EMAIL_LOCATOR_PATTERN, mailContent))).click();
    }

    public void markInboxEmail(String mailContent) {
        Logger.log.trace("Mark email inbox " + mailContent + " in " + String.format(MARK_EMAIL_LOCATOR_PATTERN, mailContent));
        driver.findElement(By.xpath(String.format(MARK_EMAIL_LOCATOR_PATTERN, mailContent))).click();
    }

    public void openEmailIn(String mailSubject) {
        Logger.log.trace("Open email in Delete List" + mailSubject + " in " + String.format(OPEN_EMAIL_LOCATOR_PATTERN, mailSubject));
        driver.findElement(By.xpath(String.format(OPEN_EMAIL_LOCATOR_PATTERN, mailSubject))).click();
    }

    public void markSpamEmail(String mailSubject) {
        Logger.log.trace("Mark email spam " + mailSubject + " in " + String.format(MARK_SPAM_EMAIL_LOCATOR_PATTERN, mailSubject));
        driver.findElement(By.xpath(String.format(MARK_SPAM_EMAIL_LOCATOR_PATTERN, mailSubject))).click();
    }

    public void downloadFile(String mailSubject) {
        Logger.log.trace("Download file  " + mailSubject + " in " + String.format(BUTTON_DOWNLOAD_LOCATOR_PATTERN, mailSubject));
        driver.findElement(By.xpath(String.format(BUTTON_DOWNLOAD_LOCATOR_PATTERN, mailSubject))).click();
    }

    public void deleteEmail() {
        Logger.log.trace("Click on " + DELETE_LINK_LOCATOR + " delete mark email");
        driver.findElement(DELETE_LINK_LOCATOR).click();
    }

    public void spamEmail() {
        Logger.log.trace("Click on " + SPAM_LINK_LOCATOR + " remove to spam mark email");
        driver.findElement(SPAM_LINK_LOCATOR).click();
    }

    public void notSpamEmail() {
        Logger.log.trace("Click on " + NOT_SPAM_LINK_LOCATOR + " remove to inbox mark email");
        driver.findElement(NOT_SPAM_LINK_LOCATOR).click();
    }

    public void logout() {
        Logger.log.trace("Logout " + CHECK_MAIL_LOCATOR + " then " + LOGOUT);
        driver.findElement(CHECK_MAIL_LOCATOR).click();
        driver.findElement(LOGOUT).click();
    }
}

package com.epam.gomel.tat.ispiridonova.bo.mail;

import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;

public enum DefaultMail {
    MAIL("test-box-IS@yandex.ru", " ", " ", null);

    private MailLetter mailLetter;

    DefaultMail(String receiver, String subject, String content, String attach) {
        mailLetter = new MailLetter(receiver, subject, content, attach);
    }

    public MailLetter getMailLetter(){return mailLetter;}
}

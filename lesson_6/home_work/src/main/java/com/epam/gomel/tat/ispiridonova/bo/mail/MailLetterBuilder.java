package com.epam.gomel.tat.ispiridonova.bo.mail;

public class MailLetterBuilder {

    public static MailLetter getMailLetter() {
        return DefaultMail.MAIL.getMailLetter();
    }
    public static MailLetter getCheckMailLetter() {
        return DefaultMail.CHECK_MAIL.getMailLetter();
    }
}

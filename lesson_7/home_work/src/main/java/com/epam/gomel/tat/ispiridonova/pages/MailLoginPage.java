package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import org.openqa.selenium.By;

public class MailLoginPage extends AbstractBasePage {

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.submit(BUTTON_ENTER_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

}

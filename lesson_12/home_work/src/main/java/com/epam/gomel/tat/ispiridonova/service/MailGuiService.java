package com.epam.gomel.tat.ispiridonova.service;

import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.pages.ReceiveMessagePage;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.utils.Utils;

import java.io.File;

public class MailGuiService {

    public void sendEmail(Letter letter) {
        Logger.trace("Send mail. Subject:  " + letter.getSubject() + " Content:  " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent());
    }

    public void sendEmailWithAttach(Letter letter) {
        Logger.trace("Send mail. Subject:  " + letter.getSubject() + " Content:  " + letter.getContent() + " Attach " + letter.getAttach());
        new MailboxBasePage()
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
    }

    public boolean isEmailInInbox(Letter letter) {
        Logger.trace("Open received mail. Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .openReceiveMessage(letter.getContent());
        boolean checkMailInbox = new ReceiveMessagePage().isCheckReceiveMessage(letter);
        Logger.trace("Check that received mail " + letter.getSubject() + " " + letter.getContent() + " is in Inbox " + checkMailInbox);
        return checkMailInbox;
    }

    public void deleteEmail(Letter letter) {
        Logger.trace("Delete mail. Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .markEmail(letter.getContent())
                .deleteEmail();
    }

    public boolean isEmailInDelete(Letter letter) {
        Logger.trace("Check mail in trash. Subject: " + letter.getSubject() + " Content: " + letter.getContent() + " in delete");
        new MailboxBasePage()
                .openDeletePage()
                .openDeleteMessage(letter.getSubject());
        boolean checkMailDeleteList = new ReceiveMessagePage()
                .isCheckReceiveMessage(letter);
        Logger.trace("Check that receive mail " + letter.getSubject() + " " + letter.getContent() + " is in Delete List " + checkMailDeleteList);
        return checkMailDeleteList;
    }

    public void removeEmailToSpam(Letter letter) {
        Logger.trace("Remove email to Spam. Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .markEmail(letter.getContent())
                .spamEmail();
      //  new MailboxBasePage().openInboxPage();
    }

    public boolean isEmailInSpam(Letter letter) {
        Logger.trace("Check mail in Spam. Subject: " + letter.getSubject() + " Content: " + letter.getContent() + " in Spam");
        new MailboxBasePage()
                .openSpamPage()
                .openEmailInSpam(letter.getSubject());
        boolean checkMailSpamList = new ReceiveMessagePage()
                .isCheckReceiveMessage(letter);
        Logger.trace("Check that receive mail " + letter.getSubject() + " " + letter.getContent() + " is in Spam " + checkMailSpamList);
        return checkMailSpamList;
    }

    public void removeEmailToInbox(Letter letter) {
        Logger.trace("Remove mail in Inbox. Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openSpamPage()
                .markEmail(letter.getSubject())
                .notSpamEmail();
    }

    public void downloadFile(Letter letter) {
        Logger.trace("Download File Subject: " + letter.getSubject());
        new MailboxBasePage()
                .openInboxPage()
                .openReceiveMessage(letter.getContent())
                .downloadFile(letter.getAttach());
        new Utils().waitForFile(letter.getAttach());
    }

    public boolean isFilesEqual(File attachFile) {
        Logger.trace("Compare Download file and Attach file is  " + new Utils().isFilesEqual(attachFile));

        return new Utils().isFilesEqual(attachFile);
    }
}

package com.epam.tat.lesson9.test.plunker;

import com.epam.tat.lesson9.browser.Browser;
import com.epam.tat.lesson9.test.BaseTest;
import com.epam.tat.lesson9.utils.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by Aleh_Vasilyeu on 3/31/2015.
 */
public class PlunkerAlertsTest extends BaseTest {

    private static final String PLNKR = "http://run.plnkr.co/plunks/VUOWcZ7iZbd7UG2yDS1A/";

    @Test(description = "Check alert")
    public void checkAlert() {
        Browser.get().open(PLNKR);
        Browser.get().waitForPresent(By.id("btn-alert"));
//        Browser.get().click(By.id("btn-alert"));
//        Browser.get().click(By.id("btn-confirm"));
        Logger.info("Text of alert: " + Browser.get().clickAndHandleAlert(By.id("btn-alert")));
        Logger.info("Text on page: " + Browser.get().getText(By.id("alert-text")));
        Browser.get().takeScreenshot();
    }

//    @Test(description = "Check alert")
//    public void checkConfirm() {
//        Browser.get().open(PLNKR);
//        Browser.get().waitForPresent(By.id("btn-confirm"));
//        Logger.info("Text of confirm: " + Browser.get().clickAndHandleConfirm(By.id("btn-confirm"), true));
//        Logger.info("Text on page: " + Browser.get().getText(By.id("confirm-text")));
//        Browser.get().takeScreenshot();
//    }
//
//    @Test(description = "Check alert")
//    public void checkPrompt() {
//        Browser.get().open(PLNKR);
//        Browser.get().waitForPresent(By.id("btn-prompt"));
//        Logger.info("Text of prompt: " + Browser.get().clickAndHandlePrompt(By.id("btn-prompt"), "MyAnswer"));
//        Logger.info("Text on page: " + Browser.get().getText(By.id("prompt-text")));
//        Browser.get().takeScreenshot();
//    }
//

}

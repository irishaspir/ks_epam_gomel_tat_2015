package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import org.openqa.selenium.By;

public class MailboxBasePage extends AbstractBasePage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SEND_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    private static final By CHECK_MAIL_LOCATOR = By.xpath("//a[contains(@id,'nb-1')]");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailSendListPage openSendPage() {
        browser.click(SEND_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSendListPage();
    }

    public MailDeleteListPage openDeletePage() {
        browser.click(DELETE_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailDeleteListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(SPAM_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public void logout() {
        browser.logout();
        browser.waitForAjaxProcessed();
    }

    public String getUserEmail() {
        return "test-box-IS@yandex.ru";
    }

    public boolean checkLogin() {
        Logger.log.trace("Check success login " + CHECK_MAIL_LOCATOR);
        return browser.isPresent(CHECK_MAIL_LOCATOR);
    }
}

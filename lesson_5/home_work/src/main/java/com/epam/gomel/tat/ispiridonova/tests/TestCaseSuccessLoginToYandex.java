package com.epam.gomel.tat.ispiridonova.tests;

import com.epam.gomel.tat.ispiridonova.pages.MailInboxListPage;
import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TestCaseSuccessLoginToYandex {

    private String userLogin = "test-box-IS";
    private String userPassword = "Qwert123";

    @Test(description = "Login and check Login")
    private void login() {
         new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
        Assert.assertTrue(new MailInboxListPage().checkLogin(), "Login is not success");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}


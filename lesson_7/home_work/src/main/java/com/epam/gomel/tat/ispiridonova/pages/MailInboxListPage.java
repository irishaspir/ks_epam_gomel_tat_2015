package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailInboxListPage extends AbstractBasePage {

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_LINK_LOCATOR);
        return new ComposeMailPage();
    }

    public MailInboxListPage openBoxMessage(String mailSubject) {
        browser.waitForElementInboxPresent(mailSubject);
        browser.openBoxEmail(mailSubject);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public ReceiveMessagePage openReceiveMessage(String mailContent) {
        browser.openReceiveEmailInInbox(mailContent);
        browser.waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }

    public MailInboxListPage markEmail(String mailContent) {
        browser.markInboxEmail(mailContent);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailInboxListPage deleteEmail() {
        browser.deleteEmail();
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailInboxListPage spamEmail() {
        browser.spamEmail();
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }
}


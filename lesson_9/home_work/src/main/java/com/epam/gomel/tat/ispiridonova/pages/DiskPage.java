package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

import java.io.File;


public class DiskPage {
    
    private static final By PROGRESS_BAR_LOCATOR = By.xpath("//div[contains(@class,'b-progressbar__fill')]");
    private static final By LOAD_BUTTON_FILE_LOCATOR = By.xpath("//input[@type='file' and contains(@class,'file')]");
    private static final By CLOSE_BUTTON_POPUP_LOCATOR = By.xpath("//button[@data-click-action='dialog.close']/span[text()='Закрыть']");
    private static final By TITLE_POPUP_LOCATOR= By.xpath("//div[contains(@class,'popup-title')]");
    private static final By FILES_LINK_LOCATOR = By.xpath("//div[contains(@class,' ns-view-visible')and contains(@data-key,'Current=disk')]//a[@href='/client/disk' and contains(@class,'item-nav')]");
    private static final By TO_MAIL_LOCATOR = By.xpath("//div[contains(@class,' ns-view-visible')and contains(@data-key,'Current=disk')]//a[contains(@href,'mail.yandex.ru')and contains(@class,'item-nav')]");
    private static final By TRASH_LINK_LOCATOR = By.xpath("//div[@data-nb='resource' and @title='Корзина']");
    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//button[contains(@data-click-action, 'resource.download') and contains(@data-params,'%s')]";
    private static final String FILE_LOCATOR_PATTERN = "//div[not(contains(@class,'hidden')) and contains(@class,'view-resource')]//div[contains(@title,'%s')]";
    private static final String MESSAGE_DOWNLOAD_FINISHED = "Загрузка завершена";

    public boolean isDisk() {
        Logger.trace("Check disk page is loaded " + FILES_LINK_LOCATOR);
        return Browser.get().isPresent(FILES_LINK_LOCATOR);
    }

    public MailboxBasePage goToPost() {
        Logger.trace("Go to mail " + TO_MAIL_LOCATOR);
        Browser.get().click(TO_MAIL_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public DiskPage downloadFile(String fileName) {
        Logger.trace("Download file");
        Browser.get().click(By.xpath(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, fileName)));
        return new DiskPage();
    }

    public DiskPage chooseFile(String fileName) {
        Logger.trace("Choose file");
        Browser.get().click(By.xpath(String.format(FILE_LOCATOR_PATTERN, fileName)));
        return new DiskPage();
    }

    public DiskPage loadFile(File file) {
        Logger.trace("Load file");
        Browser.get().attachFile(LOAD_BUTTON_FILE_LOCATOR, file.getAbsolutePath());
        Logger.trace("Wait, file is downloading ");
        String a = Browser.get().getText(TITLE_POPUP_LOCATOR);
        while (!a.equals(MESSAGE_DOWNLOAD_FINISHED)) {
            Logger.trace(a);
            System.out.println(a);
            a = Browser.get().getText(TITLE_POPUP_LOCATOR);
        }
        Browser.get().waitForVisible(CLOSE_BUTTON_POPUP_LOCATOR);
        Browser.get().click(CLOSE_BUTTON_POPUP_LOCATOR);
        return new DiskPage();
    }

    public void removeFileToTrash(String fileName) {
        Logger.trace("Remove file to trash");
        Browser.get().dragAndDrop(By.xpath(String.format(FILE_LOCATOR_PATTERN, fileName)), TRASH_LINK_LOCATOR);
        boolean progressBar = Browser.get().isPresent(PROGRESS_BAR_LOCATOR);
        while (progressBar == true) {
            progressBar = Browser.get().isPresent(PROGRESS_BAR_LOCATOR);
            Logger.trace("Wait " + progressBar);
        }
    }

    public DiskTrashPage openTrash() {
        Logger.trace("Open trash ");
        Browser.get().doubleClick(TRASH_LINK_LOCATOR);
        return new DiskTrashPage();
    }
}

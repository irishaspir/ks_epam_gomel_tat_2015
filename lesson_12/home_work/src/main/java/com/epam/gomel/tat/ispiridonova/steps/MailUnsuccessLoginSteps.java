package com.epam.gomel.tat.ispiridonova.steps;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailUnsuccessLoginSteps extends Steps {
    protected MailLoginGuiService loginService = new MailLoginGuiService();
    protected Account user;

    @BeforeStory
    public void beforeStory() {
        user = AccountBuilder.createUser();
    }

    @Given("Actor has correct login and incorrect Q$password")
    public void createCorrectLoginAndPassword(String password) {
        AccountBuilder.build(user).withCorrectLogin().withIncorrectPassword(password).withEmail();
    }

    @When("Actor  don't login to mailbox")
    public void actorNotLoginToMailbox() {
        loginService.loginToAccount(user);
    }

    @Then("Actor check error login message")
    public void actorSeeErrorLoginMessage() {
        Assert.assertTrue(loginService.isErrorMessagesRight(), "Error message isn't right");
    }
}

package com.epam.gomel.tat.ispiridonova.steps;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailSpamEmailSteps extends Steps {

    protected MailLoginGuiService loginService = new MailLoginGuiService();
    private MailGuiService mailService = new MailGuiService();
    private Letter letter = LetterBuilder.randomValues();
    protected Account user;

    @BeforeStory
    public void beforeStory() {
        user = AccountBuilder.createUser();
        AccountBuilder.build(user).withCorrectLogin().withCorrectPassword().withEmail();
    }

    @Given("Actor login and Actor send email")
    public void loginAndSendEmail() {
        loginService.loginToAccount(user);
        mailService.sendEmail(letter);
    }

    @When("Actor mark email as spam")
    public void markEmailAsSpam() {
        mailService.removeEmailToSpam(letter);
    }

    @Then("Actor see email in spam list")
    public void isEmailInSpamList() {
        Assert.assertTrue(mailService.isEmailInSpam(letter), "Email isn't in spam list");
    }
}

package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.utils.FilesUtils;
import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TestCaseSuccessSendEmailWithAttachment {

    private String userLogin = "test-box-IS"; // ACCOUNT
    private String userPassword = "Qwert123"; // ACCOUNT
    private String mailTo = "test-box-IS@yandex.ru"; // ENUM
    private String mailSubject;
    private String mailContent;
    private String attachFile = null;
    private String downloadFile;
    private String pathAttachFile = "D:\\temp\\";
    private String pathDownloadFile = "d:\\_TEMP\\";
    private String typeFile = ".txt";
    private int fileNameLetter = 10; //
    private int contentLetter = 20;

    @Test(description = "Login")
    public void login() {
        new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
    }

    @Test(description = "Send letter with attachment ", dependsOnMethods = "login")
    public void sendLetter() {
        RandomUtils generateRandom = new RandomUtils();
        mailSubject = generateRandom.subject(fileNameLetter);
        mailContent = generateRandom.content(contentLetter);
        FilesUtils createFile = new FilesUtils();
        attachFile = createFile.createFile(pathAttachFile, mailSubject, mailContent, typeFile);
        new MailboxBasePage()
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent, attachFile);
    }

    @Test(description = "Download file", dependsOnMethods = "sendLetter")
    public void downloadFile() {
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(mailSubject)
                .openReceiveMessage(mailContent)
                .downloadFile(mailSubject);
    }

    @Test(description = "Compare files", dependsOnMethods = "downloadFile")
    public void compareFiles() {
        downloadFile = pathDownloadFile + mailSubject + typeFile;
        FilesUtils compareFile = new FilesUtils();
        compareFile.compareFiles(attachFile, downloadFile);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}

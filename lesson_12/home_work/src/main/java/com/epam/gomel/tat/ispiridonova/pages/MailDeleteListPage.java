package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

public class MailDeleteListPage{

    private static final String EMAIL_LOCATOR_PATTERN = "//*[contains(@title,'%s')]/ancestor::a[contains(@href,'#message')]";

    public ReceiveMessagePage openDeleteMessage(String mailSubject) {
        Browser.get().click(By.xpath(String.format(EMAIL_LOCATOR_PATTERN,mailSubject)));
        Browser.get().waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }
}

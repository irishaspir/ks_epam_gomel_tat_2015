package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

public class MailSpamListPage {

    private static final By NOT_SPAM_LINK_LOCATOR = By.xpath("//a[@data-action='notspam']");
    private static final String EMAIL_LOCATOR_PATTERN = "//div[contains(@class,'block-messages-wrap') and not(contains(@style,'none'))]//*[contains(@title,'%s')]/ancestor::a";
    private static final String CHECKBOX_EMAIL_LOCATOR_PATTERN = "//span[@title='%s']/ancestor::div[contains(@class, 'js-message')and not(contains(@style,'none'))]//input[@class='b-messages__message__checkbox__input']";

    public MailSpamListPage markEmail(String mailSubject) {
        Browser.get().waitForVisible(By.xpath(String.format(EMAIL_LOCATOR_PATTERN, mailSubject)));
        Browser.get().click(By.xpath(String.format(CHECKBOX_EMAIL_LOCATOR_PATTERN, mailSubject)));
        Browser.get().waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public MailSpamListPage notSpamEmail() {
        Browser.get().waitForVisible(NOT_SPAM_LINK_LOCATOR);
        Browser.get().click(NOT_SPAM_LINK_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public ReceiveMessagePage openEmailInSpam(String mailSubject) {
        Browser.get().waitForVisible(By.xpath(String.format(EMAIL_LOCATOR_PATTERN, mailSubject)));
        Browser.get().click(By.xpath(String.format(EMAIL_LOCATOR_PATTERN, mailSubject)));
        Browser.get().waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }
}

package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import org.openqa.selenium.By;


public abstract class AbstractBasePage {

    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By BUTTON_ENTER_LOCATOR = By.xpath("//div[@class='new-left']//button[@type='submit']");

    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By ATTACH_FILE_LOCATOR = By.xpath("//input[@name='att']");

    public static final By CHECK_MESSAGE_MISTAKE_LOCATOR = By.xpath("//div[contains(@class, 'js-messages')]//div[contains(@class, 'error-msg')]");

    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By SEND_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By DELETE_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    public static final By CHECK_MAIL_LOCATOR = By.xpath("//a[contains(@id,'nb-1')]");

    public static final By COMPOSE_LINK_LOCATOR = By.xpath("//a[@href='#compose']");

    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";

    public static final String EMAIL_CHECK_LOCATOR_PATTERN = "//span[@title='%s']";

    public static final String MAIL_SUBJECT_LOCATOR_PATTERN = "//div[contains(@class,'message-subject')]//span[text()='%s']";
    public static final String MAIL_CONTENT_LOCATOR_PATTERN = "//div[contains(@class,'block-message-body-box')]//*[contains(text(),'%s')]";
    public static final String MAIL_ADRESS_LOCATOR_PATTERN = "//div[contains(@class,'b-message-head__field__content')]//*[contains(text(),'%s')]";
    public static final String MAIL_ATTACH_LOCATOR_PATTERN = "//div[@class='b-message-head__field_from__i']//*[contains(text(),'%s')]";

    public static final String BASE_URL = GlobalConfig.getBASEURL();

    protected Browser browser;

    public AbstractBasePage() {
        this.browser = Browser.get();
    }
}

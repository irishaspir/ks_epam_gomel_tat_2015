package com.epam.gomel.tat.ispiridonova.cli;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.util.List;

public class TestRunnerOptions {

    @Option(name = "-bt", usage = "browser type", required = true)
    public String browserType;

    @Option(name = "-localhost", depends = {"-bt == firefox*"})
    public String localHost;

    @Option(name = "-port", depends = {"-bt == firefox*"})
    public String port;

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    public String parallelMode;

    @Option(name = "-tc", usage = "thread count", depends = {"-mode != false"})
    public int threadCount;

    @Option(name = "-suits", handler = StringArrayOptionHandler.class)
    public List<String> suits;

}

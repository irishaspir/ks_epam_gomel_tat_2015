package com.epam.gomel.tat.ispiridonova.bo.common;

/**
 * Created by Ira on 23.07.2015.
 */
public class AccountBuilder {
    public static Account getDefaultAccount() {
        return DefaultAccounts.DEFAULT_MAIL.getAccount();
    }

    public static Account getUnsuccessMail() {
        return DefaultAccounts.UNSUCCESS_MAIL.getAccount();
    }
}

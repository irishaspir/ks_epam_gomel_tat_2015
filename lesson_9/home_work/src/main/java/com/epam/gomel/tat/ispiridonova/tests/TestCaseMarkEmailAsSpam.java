package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.Credentials;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseMarkEmailAsSpam {

    private MailLoginGuiService loginService = new MailLoginGuiService();
    private MailGuiService mailService = new MailGuiService();
    private Account credentials = Credentials.RIGHT_CREDENTIALS.getCredentials();
    private Letter letter = LetterBuilder.randomValues();

    @Test(description = "Login")
    public void login() {
        loginService.loginToAccount(credentials);
        Assert.assertTrue(loginService.isSuccessLogin(credentials), "Login is unsuccess");
    }

    @Test(description = "Send mailService", dependsOnMethods = "login")
    public void sendEmail() {
        mailService.sendEmail(letter);
        Assert.assertTrue(mailService.isEmailInInbox(letter), "Email isn't in Inbox");
    }

    @Test(description = "Mark Email as spam letter", dependsOnMethods = "sendEmail")
    public void removeEmailToSpam() {
        mailService.removeEmailToSpam(letter);
        Assert.assertTrue(mailService.isEmailInSpam(letter), "Email isn't in Spam");
    }

    @Test(description = "Logout", dependsOnMethods = "removeEmailToSpam")
    public void logoutEmail() {
        loginService.logout();
    }
}

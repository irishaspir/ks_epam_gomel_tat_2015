package com.epam.gomel.tat.ispiridonova.bo.mail;

import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;

public class MailLetter {
    private String receiver;
    private String subject;
    private String content;
    private String attach;
    RandomUtils generateRandom = new RandomUtils();

    public MailLetter(String receiver, String subject, String content, String attach) {
        this.receiver = receiver;
        this.subject = subject;
        this.content = content;
        this.attach = attach;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return  subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

}

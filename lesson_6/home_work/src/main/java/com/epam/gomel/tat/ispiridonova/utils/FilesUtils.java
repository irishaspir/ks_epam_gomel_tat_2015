package com.epam.gomel.tat.ispiridonova.utils;

import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class FilesUtils {

    private String pathAttachFile = "D:\\temp\\";
    private String pathDownloadFile = "d:\\_TEMP\\";
    private String typeFile = ".txt";
    private File file;

    private static final Logger LOGGER = Logger.getLogger(FilesUtils.class);
    private boolean resultEqual;

    public String createFile(MailLetter letter) {
        file = org.apache.commons.io.FileUtils.getFile(this.pathAttachFile, letter.getSubject() + this.typeFile);
        try {
            org.apache.commons.io.FileUtils.write(file, letter.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    public boolean compareFiles(MailLetter letter) {
        try {
            File file1 = new File(this.pathAttachFile + letter.getSubject() + this.typeFile);
            File file2 = new File(this.pathDownloadFile + letter.getSubject() + this.typeFile);
          resultEqual =  org.apache.commons.io.FileUtils.contentEquals(file1, file2);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return resultEqual;
    }
}

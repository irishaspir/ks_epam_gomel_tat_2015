package com.epam.gomel.tat.ispiridonova;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test_Case_Unsuccess_Login_to_Yanex_mail {

    // AUT data
    public static final String BASE_URL = "http://www.yandex.ru";
    public static final String MESSAGE_MISTAKE = "Неправильный логин или пароль.";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[descendant::span[contains(@class, 'button__text')]]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By CHECK_MESSAGE_MISTAKE_LOCATOR = By.xpath("//div[contains(@class, 'js-messages')]//div[contains(@class, 'error-msg')]");

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    // Test data
    private String userLogin = "test-box-IS"; // ACCOUNT
    private String userPassword = "Qwert12"; // ACCOUNT


    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }


    @Test(description = "Success mail login")
    public void login() {

       // login
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();


        //check error and text mistake message
        WebElement checkMessageMistake = driver.findElement(CHECK_MESSAGE_MISTAKE_LOCATOR);
        checkMessageMistake.isEnabled();
        Assert.assertEquals(checkMessageMistake.getText(), MESSAGE_MISTAKE);

        }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

    }






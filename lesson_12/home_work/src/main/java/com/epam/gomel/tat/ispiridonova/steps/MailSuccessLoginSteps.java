package com.epam.gomel.tat.ispiridonova.steps;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.jbehave.core.annotations.*;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailSuccessLoginSteps extends Steps {

    protected MailLoginGuiService loginService = new MailLoginGuiService();
    protected Account user;

    @BeforeStory
    public void beforeStory() {
        user = AccountBuilder.createUser();
    }

    @Given("Actor has correct login and correct password")
    public void createCorrectLoginAndCorrectPassword() {
        AccountBuilder.build(user).withCorrectLogin().withCorrectPassword().withEmail();
    }

    @When("Actor login to mailbox")
    public void actorLoginToMailbox() {
        loginService.loginToAccount(user);
    }

    @Then("Actor has access to mail list")
    public void actorHasAccessToMailList() {
        Assert.assertTrue(loginService.isSuccessLogin(user), "Login is unsuccess");
    }
}

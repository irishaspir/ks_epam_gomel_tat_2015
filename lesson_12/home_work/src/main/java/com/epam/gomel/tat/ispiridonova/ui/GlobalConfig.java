package com.epam.gomel.tat.ispiridonova.ui;


import org.apache.commons.io.FileUtils;

public class GlobalConfig {

    private static final String BASE_URL = "http://www.mail.yandex.ru";

    private static String downloadDirFirefox = FileUtils.getTempDirectoryPath() + "\\DownloadDir";

    private static String attachDir = FileUtils.getTempDirectoryPath() + "\\AttachDir";

    private static String typeFile = ".txt";

    private static int length = 10;

    private static final int WAIT_FILE_DOWNLOAD_SECONDS = 20;

    public static String getDownloadDirFirefox() {
        return downloadDirFirefox;
    }

    public static String getAttachDir() {
        return attachDir;
    }

    public static void setDownloadDirFirefox(String downloadDirFirefox) { GlobalConfig.downloadDirFirefox = downloadDirFirefox;}

    public static String getFileType() {
        return typeFile;
    }

    public static String getBASEURL() {
        return BASE_URL;
    }

    public static int getLength() {
        return length;
    }

    public static void setLength(int length) { GlobalConfig.length = length; }

    public static int getWaitFileDownloadSeconds(){return WAIT_FILE_DOWNLOAD_SECONDS;}
}

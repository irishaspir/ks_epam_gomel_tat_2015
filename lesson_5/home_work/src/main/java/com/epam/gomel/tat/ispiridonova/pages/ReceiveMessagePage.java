package com.epam.gomel.tat.ispiridonova.pages;

public class ReceiveMessagePage extends AbstractBasePage {

    public void downloadFile(String mailSubject) {
        browser.downloadFile(mailSubject);
    }
}

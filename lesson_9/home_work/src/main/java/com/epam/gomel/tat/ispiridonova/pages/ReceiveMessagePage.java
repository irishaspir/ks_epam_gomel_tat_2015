package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

import java.io.File;

public class ReceiveMessagePage {

    public static final String SUBJECT_LOCATOR_PATTERN = "//div[contains(@class,'message-subject')]//span[text()='%s']";
    public static final String CONTENT_LOCATOR_PATTERN = "//div[contains(@class,'block-message-body-box')]//*[contains(text(),'%s')]";
    public static final String ADDRESS_LOCATOR_PATTERN = "//div[contains(@class,'b-message-head__field__content')]//*[contains(text(),'%s')]";
    public static final String ATTACH_LINK_LOCATOR_PATTERN = "//div[@class='b-message-head__field_from__i']//*[contains(text(),'%s')]";
    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//*[contains(text(),'%s')]//ancestor::div[contains(@class, 'b-message-attachments_head')]//a[contains(@href,'message_part')]";

    public boolean isCheckReceiveMessage(Letter letter) {
        Browser.get().isPresent(By.xpath(String.format(SUBJECT_LOCATOR_PATTERN, letter.getSubject())));
        Browser.get().isPresent(By.xpath(String.format(CONTENT_LOCATOR_PATTERN, letter.getContent())));
        Browser.get().isPresent(By.xpath(String.format(ADDRESS_LOCATOR_PATTERN, letter.getReceiver())));
        if (letter.getReceiver() != null) {
            Browser.get().isPresent(By.xpath(String.format(ATTACH_LINK_LOCATOR_PATTERN, letter.getReceiver())));
        }
        return true;
    }

    public void downloadFile(File file) {
        Browser.get().click(By.xpath(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, file.getName())));
    }
}

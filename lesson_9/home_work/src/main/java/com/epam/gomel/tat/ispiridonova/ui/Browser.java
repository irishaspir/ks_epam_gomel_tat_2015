package com.epam.gomel.tat.ispiridonova.ui;

import atu.testng.reports.ATUReports;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.utils.Utils;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class Browser {

    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 60;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 40;
    private static final int AJAX_TIMEOUT = 40;
    private static final int WAIT_ELEMENT_TIMEOUT = 60;
    private static final int TIME_OUT_IN_SECONDS = 40;

    private static final String FILE_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, text/csv, application/octet-stream, application/txt, image/jpeg";
    private static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private boolean augmented;

    private WebDriver driver;

    private static Map<Thread, Browser> instances = new HashMap<>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = GlobalConfig.getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                Logger.trace("Open Firefox: pageLoadTimeout " + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS + " host: " + GlobalConfig.getLocalHost() + " port: " + GlobalConfig.getPort());
                try {
                    driver = new RemoteWebDriver(new URL("http://" + GlobalConfig.getLocalHost() + ":" + GlobalConfig.getPort() + "/wd/hub"),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }
                break;
            case FIREFOX:
                Logger.trace("Open Firefox: pageLoadTimeout " + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS);
                driver = new FirefoxDriver(getFireFoxProfile());
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", Utils.getCanonicalPathToResourceFile("/chromedriver.exe"));
                Logger.trace("Open Chrome: pageLoadTimeout " + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS);
                driver = new ChromeDriver(getChromeOption());
                break;
        }
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        Logger.trace("Download dir " + GlobalConfig.getDownloadDirFirefox() + " Type files to save " + FILE_TYPES_TO_SAVE);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalConfig.getDownloadDirFirefox());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FILE_TYPES_TO_SAVE);
        return profile;
    }

    private static DesiredCapabilities getChromeOption() {
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", GlobalConfig.getDownloadDirChrome());
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--test-type");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        return cap;
    }

    public void open(String url) {
        Logger.trace("Open url " + url);
        driver.get(url);
        ATUReports.setWebDriver(driver);
    }

    public void killBrowser() {
        Logger.trace("Close Browser");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void click(By locator) {
        Logger.trace("Click on element " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.click();
        takeScreenshot();
    }

    public void type(By locator, String text) {
        Logger.trace("Type text " + text + " in " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.clear();
        element.sendKeys(text);
        takeScreenshot();
    }

    public void attachFile(By locator, String text) {
        Logger.trace("Type to " + locator + "  path of file " + text);
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isPresent(By locator) {
        Logger.trace("Find element " + locator);
        return driver.findElements(locator).size() > 0;
    }

    public String getText(By locator) {
        Logger.trace("Taken message " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator).getText();
    }

    public void waitElementIsPresent(final By locator) {
        Logger.trace("Wait " + TIME_OUT_IN_SECONDS + " element " + locator + " is present in Inbox");
        new WebDriverWait(driver, TIME_OUT_IN_SECONDS).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.trace("Wait " + WAIT_ELEMENT_TIMEOUT + " element " + locator + " is visible");
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForInvisible(By locator) {
        Logger.trace("Wait " + WAIT_ELEMENT_TIMEOUT + " element " + locator + " is invisible");
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void submit(By locator) {
        Logger.trace("Click on " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.submit();
        takeScreenshot();
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

    public void takeScreenshot() {
        if (GlobalConfig.getBrowserType() == BrowserType.REMOTE &&
                !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public void dragAndDrop(By locatorDrag, By locatorDrop) {
        WebElement dragElementFrom = driver.findElement(locatorDrag);
        WebElement dropElementTo = driver.findElement(locatorDrop);
        elementHighlight(dragElementFrom);
        elementHighlight(dropElementTo);
        takeScreenshot();
        Actions builder = new Actions(driver);
        Action dragAndDrop = builder.clickAndHold(dragElementFrom)
                .moveToElement(dropElementTo)
                .release(dropElementTo)
                .build();
        dragAndDrop.perform();
    }

    public void doubleClick(By locator) {
        WebElement element = driver.findElement(locator);
        new Actions(driver).doubleClick(element).build().perform();
    }

}

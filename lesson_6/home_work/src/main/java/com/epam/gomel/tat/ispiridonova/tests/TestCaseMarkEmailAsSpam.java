package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.service.LoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.LogoutGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseMarkEmailAsSpam {

    LoginGuiService loginEmail = new LoginGuiService();
    LogoutGuiService logout = new LogoutGuiService();
    MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getMailLetter();


    @Test(description = "login")
    public void loginSuccess() {
        Logger.log.trace("TestCaseMarkEmailAsSpam");
        loginEmail.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail", dependsOnMethods = "loginSuccess")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check email in Inbox", dependsOnMethods = "sendMail")
    public void checkEmailInInbox() {
        Assert.assertTrue(mailGuiService.checkMailInInbox(letter), "Email is not Inbox");
    }

    @Test(description = "Spam Letter", dependsOnMethods = "checkEmailInInbox")
    public void removeEmailToSpam() {
        mailGuiService.removeEmailToSpam(letter);
    }

    @Test(description = "Check email in Spam", dependsOnMethods = "removeEmailToSpam")
    public void checkEmailInSpamList() {
        Assert.assertTrue(mailGuiService.checkMailInSpam(letter), "Email is not in Spam");
    }

    @Test(description = "logout", dependsOnMethods = "checkEmailInSpamList")
    public void logoutEmail() {
        logout.logout();
    }
}

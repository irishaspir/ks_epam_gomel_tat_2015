package com.epam.gomel.tat.ispiridonova.bo.mail;


import java.io.File;

public class Letter {
    private String receiver;
    private String subject;
    private String content;
    private File attachFile;

    public Letter(String receiver, String subject, String content, File attach) {
        this.receiver = receiver;
        this.subject = subject;
        this.content = content;
        this.attachFile = attach;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return  subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public File getAttach() {
        return attachFile;
    }

    public void setAttach(File attach) {
        this.attachFile = attach;
    }

}

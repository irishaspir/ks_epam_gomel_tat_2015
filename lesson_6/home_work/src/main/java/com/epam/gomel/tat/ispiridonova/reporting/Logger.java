package com.epam.gomel.tat.ispiridonova.reporting;

public class Logger {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);


    public static void error(String s) {
        log.error(s);
    }

    public static void error(String s, Throwable e) {
        log.error(s, e);
    }

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam");

    public static void info(String message) {
        logger.info(message);
    }

}

package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.service.LoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.LogoutGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseSuccessSendEmailWithAttachment {

    LoginGuiService loginGuiService = new LoginGuiService();
    LogoutGuiService logout = new LogoutGuiService();
    MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getMailLetter();

    @Test(description = "Login")
    public void login() {
        Logger.log.trace("TestCaseSuccessSendEmailWithAttachment");
        loginGuiService.loginToAccountMailbox(defaultAccount);
        Assert.assertTrue(loginGuiService.isLoginSuccess(), "login unsuccess");
    }

    @Test(description = "Send mail", dependsOnMethods = "login")
    public void sendMail() {
        mailGuiService.sendMailWithAttach(letter);
        Assert.assertTrue(mailGuiService.isMailInInbox(letter), "Email is not Inbox");
    }

    @Test(description = "Download file from receive mail",dependsOnMethods = "sendMail")
    public void downloadFile(){
        mailGuiService.downloadFile(letter);
    }

    @Test(description = "Compare Files Download And Attach",dependsOnMethods = "downloadFile")
    public void compareFilesDownloadAndAttach(){
        Assert.assertTrue(mailGuiService.isFilesEquivalent(letter), "Files not Equals");
    }

    @Test(description = "logout", dependsOnMethods = "compareFilesDownloadAndAttach")
    public void logoutEmail() {
        logout.logout();
    }

}

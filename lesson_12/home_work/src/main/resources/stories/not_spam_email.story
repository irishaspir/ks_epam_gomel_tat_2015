Feature: Mark email as not spam

Meta:
@Mark email as not spam

Narrative:
As a Mailbox user
I want to login in mailbox, send email, mark email as apam and return it in inbox
So that I have access to mailbox list and  I see returned email in inbox

Scenario: mark email as not spam
Given Actor is in inbox and Actor send email
When Actor mark email as spam and return it in inbox
Then Actor see returned email in inbox
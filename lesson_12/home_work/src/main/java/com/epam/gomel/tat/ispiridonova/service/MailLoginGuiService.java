package com.epam.gomel.tat.ispiridonova.service;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import org.testng.Assert;

public class MailLoginGuiService {

    private static final String ERROR_LOGIN_MESSAGE = "Неправильный логин или пароль.";
    public void loginToAccount(Account account) {
        Logger.trace("Login to account " + account.getLogin());

        new MailLoginPage()
                .open()
                .login(account.getLogin(), account.getPassword());
    }

    public boolean isSuccessLogin(Account account) {
        Logger.trace("Check login");
        boolean login = new MailboxBasePage().isCheckLogin();
        if (login) {
            String userEmail = new MailboxBasePage().getUserEmail();
            if (!userEmail.equals(account.getEmail())) {
                        throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
            }
        }
        return login;
    }

    public void logout() {
        Logger.trace("Logout ");
        new MailboxBasePage().logout();
    }

    public boolean isErrorMessagesRight() {
        String errorLogin = new MailLoginPage()
                .errorMessage();
        Logger.trace("Check  message  " + errorLogin + " is equal message  " + ERROR_LOGIN_MESSAGE);
        Assert.assertEquals(errorLogin, ERROR_LOGIN_MESSAGE);
        boolean equal = errorLogin.equals(ERROR_LOGIN_MESSAGE);
        return equal ;
    }
}

package com.epam.gomel.tat.ispiridonova;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Test_Case_Success_send_email_with_attachment {

    // AUT data
    public static final String BASE_URL = "http://www.yandex.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[descendant::span[contains(@class, 'button__text')]]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By ATTACH_FILE_LOCATOR = By.xpath("//input[@name='att']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";
    public static final String RECEIVE_LETTER_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-messages-item')]//div[not(.//span[@class='b-messages__folder'])]//a[contains(@href, '#message')]";
    public static final String BUTTON_DOWNLOAD_LOCATOR_PATTERN = "//*[contains(text(),'%s')]//ancestor::div[contains(@class, 'b-message-attachments_head')]//a[contains(@href,'message_part')]";
    public static final String FIREFOX_TYPES_TO_SAVE = "text/plain";

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 60;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 60;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    public static final int DOWNLOAD_FILE_WAIT_TIMEOUT_SECONDS = 10;
    private WebDriver driver;

    // Test data
    private String userLogin = "test-box-IS"; // ACCOUNT
    private String userPassword = "Qwert123"; // ACCOUNT
    private String mailTo = "test-box-IS@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM
    private String attachFile = "D:\\" + mailSubject + ".txt"; // attach file
    private String downloadFile = "C:\\downloads\\" + mailSubject + ".txt"; //download file

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setEnableNativeEvents(true);
        profile.setPreference("javascript.enabled", true);
        profile.setPreference("dom.max_script_run_time", 0);
        profile.setPreference("dom.max_chrome_script_run_time", 0);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", "c:\\downloads");
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_TYPES_TO_SAVE);
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success mail login")
    public void login() {

        //login
         LoginMail();

        //send mail
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);


        // attach file
        CreateFile(); //create file to attach
        WebElement attachFileButton = waitForElement(ATTACH_FILE_LOCATOR);
        attachFileButton.sendKeys(attachFile);

        // send mail
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        //check letter
        WebElement inboxLink = driver.findElement(INPOX_LINK_LOCATOR);
        inboxLink.click();

        //we wait (TIME_OUT_MAIL_ARRIVED_SECONDS) this time until element is Visible element =(MAIL_LINK_LOCATOR_PATTERN+mailSubject)
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));

        //open box whit letters
        WebElement openBoxWithLettes = driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        openBoxWithLettes.click();

        //open received letter
        WebElement openResiveLetter = driver.findElement(By.xpath(String.format(RECEIVE_LETTER_LOCATOR_PATTERN, mailContent)));
        openResiveLetter.click();

        //download file
        WebElement downloadFile = driver.findElement(By.xpath(String.format(BUTTON_DOWNLOAD_LOCATOR_PATTERN, mailSubject)));
        downloadFile.click();

        //wait until file download
         try {
            Thread.sleep(DOWNLOAD_FILE_WAIT_TIMEOUT_SECONDS * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();}

        //compare two files
        compareFiles();
    }


    public void LoginMail(){
        //login in mail
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();    }

    public void CreateFile(){

        File newFile = new File(attachFile);
        try{
        if (newFile.createNewFile()) {System.out.println("Файл создан");}
        else {System.out.println("Файл уже существует");}}
        catch (IOException e) {
            e.printStackTrace();}
        FileWriter writeInFile = null;
        try {
            writeInFile = new FileWriter(newFile);
            writeInFile.write(mailContent);
        } catch (IOException e) {
            e.printStackTrace();}
        finally {
            if(writeInFile != null) {
                try {
                    writeInFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void compareFiles(){
        try{
            File file1= new File(attachFile);
            if (!file1.exists()) Assert.fail(attachFile + " doesn't exist.");
            if (!file1.isFile()) Assert.fail(attachFile+" is not a file.");
            File file2= new File(downloadFile);
            if (!file2.exists()) Assert.fail(downloadFile+" doesn't exist.");
            if (!file2.isFile()) Assert.fail(downloadFile+" is not a file.");
            Assert.assertTrue(org.apache.commons.io.FileUtils.contentEquals(file1, file2), "Content of the files are not equal");}
        catch  (IOException ex){
            ex.printStackTrace();}
    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);    }

      @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

}

package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class ErrorLoginPage extends AbstractBasePage {

    public String errorMessage() {
        return browser.errorMessage(CHECK_MESSAGE_MISTAKE_LOCATOR);
    }
}

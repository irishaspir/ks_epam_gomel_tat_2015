package com.epam.tat.lesson9.test.plunker;

import com.epam.tat.lesson9.browser.Browser;
import com.epam.tat.lesson9.test.BaseTest;
import com.epam.tat.lesson9.utils.Timeout;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by Aleh_Vasilyeu on 3/31/2015.
 */
public class PlunkerActionsTests extends BaseTest {

    private static final String PLNKR = "http://run.plnkr.co/plunks/VUOWcZ7iZbd7UG2yDS1A/";

    @Test
    public void testDragAndDropAction() {
        Browser.get().open(PLNKR);
        Browser.get().waitForPresent(By.id("draggable"));
        Browser.get().takeScreenshot();
        Browser.get().dragAndDrop(By.id("draggable"), 100, 60);
        Browser.get().takeScreenshot();
        Timeout.sleep(2);
    }

    @Test
    public void testMouseOver() {
        Browser.get().open(PLNKR);
        Browser.get().waitForPresent(By.id("container"));
        Browser.get().takeScreenshot();
        Browser.get().mouseOver(By.id("container"));
        Browser.get().takeScreenshot();
        Timeout.sleep(2);
    }

    @Test
    public void testDblClick() {
        Browser.get().open(PLNKR);
        Browser.get().waitForPresent(By.id("draggable"));
        Browser.get().doubleClick(By.id("draggable"));
        Browser.get().takeScreenshot();
        Timeout.sleep(2);
    }
}

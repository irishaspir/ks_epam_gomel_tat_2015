package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailSpamListPage extends AbstractBasePage {

    public static final String EMAIL_CHECK_LOCATOR_PATTERN = "//span[@title='%s']";

    public boolean isMessagePresent(String subject) {
        return browser.isPresent(By.xpath(String.format(EMAIL_CHECK_LOCATOR_PATTERN, subject)));
    }

    public MailSpamListPage markEmail(String mailSubject) {
        browser.waitForAjaxProcessed();
        browser.markSpamEmail(mailSubject);
        return new MailSpamListPage();
    }

    public MailSpamListPage notSpamEmail(){
        browser.notSpamEmail();
        return new MailSpamListPage();
    }
}

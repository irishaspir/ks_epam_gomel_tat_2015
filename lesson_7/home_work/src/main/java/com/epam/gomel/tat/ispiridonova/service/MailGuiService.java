package com.epam.gomel.tat.ispiridonova.service;

import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.pages.ReceiveMessagePage;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import com.epam.gomel.tat.ispiridonova.utils.MyUtils;
import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;

public class MailGuiService {

    private int x = GlobalConfig.getLenght();

    public void sendMail(MailLetter letter) {
        Logger.log.trace("Send mail");
        letter.setSubject(new RandomUtils().subject(x));
        letter.setContent(new RandomUtils().content(x));
        Logger.log.trace("Send mail Subject:  " + letter.getSubject() + " Content:  " + letter.getContent());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent());
    }

    public void sendMailWithAttach(MailLetter letter) {
        Logger.log.trace("Send mail with attach");
        letter.setSubject(new RandomUtils().subject(x));
        letter.setContent(new RandomUtils().content(x));
        letter.setAttach(new RandomUtils().subject(x));
        Logger.log.trace("Create file with name:  " + letter.getSubject());
        letter.setAttach(new MyUtils().createFile(letter));
        Logger.log.trace("Send mail Subject:  " + letter.getSubject() + " Content:  " + letter.getContent() + " Attach " + letter.getAttach());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
    }

    public boolean isMailInInbox(MailLetter letter) {
        Logger.log.trace("Open receive mail Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .openReceiveMessage(letter.getContent());
        boolean checkMailInbox = new ReceiveMessagePage().isCheckReceiveMessage(letter);
        Logger.log.trace("Check that receive mail " + letter.getSubject() + " " + letter.getContent() + " is in Inbox " + checkMailInbox);
        return checkMailInbox;
    }

    public void deleteMail(MailLetter letter) {
        Logger.log.trace("Delete mail Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .markEmail(letter.getContent())
                .deleteEmail();
    }

    public boolean isMailInDelete(MailLetter letter) {
        Logger.log.trace("Check mail Subject: " + letter.getSubject() + " Content: " + letter.getContent() + " in delete");
        new MailboxBasePage()
                .openDeletePage()
                .openDeleteMessage(letter.getSubject());
        boolean checkMailDeleteList = new ReceiveMessagePage()
                .isCheckReceiveMessage(letter);
        Logger.log.trace("Check that receive mail " + letter.getSubject() + " " + letter.getContent() + " is in Delete List " + checkMailDeleteList);
        return checkMailDeleteList;
    }

    public void removeEmailToSpam(MailLetter letter) {
        Logger.log.trace("Remove mail to Spam Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .markEmail(letter.getContent())
                .spamEmail();
    }

    public boolean isMailInSpam(MailLetter letter) {
        Logger.log.trace("Check mail iSubject: " + letter.getSubject() + " Content: " + letter.getContent() + " in Spam");
        new MailboxBasePage()
                .openSpamPage()
                .openEmailInSpam(letter.getSubject());
        boolean checkMailSpamList = new ReceiveMessagePage()
                .isCheckReceiveMessage(letter);
        Logger.log.trace("Check that receive mail " + letter.getSubject() + " " + letter.getContent() + " is in Spam " + checkMailSpamList);
        return checkMailSpamList;
    }

    public void removeMailInToIbox(MailLetter letter) {
        Logger.log.trace("Remove mail to Inbox Subject: " + letter.getSubject() + " Content: " + letter.getContent());
        new MailboxBasePage()
                .openSpamPage()
                .markEmail(letter.getSubject())
                .notSpamEmail();
    }

    public void downloadFile(MailLetter letter){
        Logger.log.trace("Download File Subject: " + letter.getSubject());
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(letter.getSubject())
                .openReceiveMessage(letter.getContent())
                .downloadFile(letter.getSubject());
        new MyUtils().waitForFile(letter);
    }

    public boolean isFilesEquivalent(MailLetter letter){
        Logger.log.trace("Compare Download file and Attach file is  " + new MyUtils().isCompareFiles(letter));
        return  new MyUtils().isCompareFiles(letter);
    }
}

package com.epam.gomel.tat.ispiridonova.exception;

public class TestCommonRuntimeException extends RuntimeException{

    public TestCommonRuntimeException(String s) {
        super(s);
    }

    public TestCommonRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }


}

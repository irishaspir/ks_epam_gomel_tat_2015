package com.epam.tat.lesson9;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public enum ParallelMode {

    FALSE("false"),
    TESTS("tests"),
    CLASSES("classes"),
    METHODS("methods"),
    SUITES("suites");

    private String alias;

    private ParallelMode(String alias) {
        this.alias = alias;
    }

    public static ParallelMode getTypeByAlias(String alias) {
        for(ParallelMode type: ParallelMode.values()){
            if(type.getAlias().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }
}

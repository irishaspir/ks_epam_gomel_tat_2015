package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import org.openqa.selenium.By;

public class MailSendListPage extends AbstractBasePage {

    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMessagePresent(MailLetter letter) {
        return browser.isPresent(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
    }
}

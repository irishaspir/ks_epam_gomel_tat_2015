package com.epam.gomel.tat.ispiridonova.tests;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.service.LoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.LogoutGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestCaseSuccessLogin {

    LoginGuiService loginEmail = new LoginGuiService();
    LogoutGuiService logout = new LogoutGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @Test(description = "login")
    public void loginSuccess() {
        Logger.log.trace("TestCaseSuccessLogin");
        loginEmail.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "chek login",dependsOnMethods = "loginSuccess")
    public void checkLogin(){
       Assert.assertTrue(loginEmail.checkLoginSuccess(), "login unsuccess");
    }

    @Test(description = "logout",dependsOnMethods = "checkLogin")
    public void logoutEmail(){
        logout.logout();
    }
}


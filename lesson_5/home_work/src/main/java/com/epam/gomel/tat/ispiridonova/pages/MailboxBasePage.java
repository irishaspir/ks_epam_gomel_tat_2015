package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailboxBasePage extends AbstractBasePage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SEND_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SPAM_LINK_LOCATOR = By.xpath( "//a[@href='#spam']");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage();
    }

    public MailSendListPage openSendPage() {
        browser.click(SEND_LINK_LOCATOR);
        return new MailSendListPage();
    }

    public MailDeleteListPage openDeletePage(){
        browser.waitForAjaxProcessed();
        browser.click(DELETE_LINK_LOCATOR);
        return new MailDeleteListPage();
    }

    public MailSpamListPage openSpamPage(){
        browser.waitForAjaxProcessed();
        browser.click(SPAM_LINK_LOCATOR);
        return new MailSpamListPage();
    }
}

package com.epam.gomel.tat.ispiridonova.utils;

import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import com.google.common.base.Function;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class MyUtils {

    private String pathAttachFile = FileUtils.getTempDirectoryPath() + "\\AttachDir";
    private String pathDownloadFile = GlobalConfig.getDownloadDir();
    private String typeFile = GlobalConfig.getTypeFile();
    private static final int WAIT_FILE_DOWNLOAD_SECONDS = GlobalConfig.getWaitFileDownloadSeconds();
    private File file;

    private static final Logger LOGGER = Logger.getLogger(MyUtils.class);
    private boolean resultEqual;

    public String createFile(MailLetter letter) {
        new File(FileUtils.getTempDirectoryPath() + "\\AttachDir").mkdir();
        file = org.apache.commons.io.FileUtils.getFile(this.pathAttachFile, letter.getSubject() + this.typeFile);
        try {
            org.apache.commons.io.FileUtils.write(file, letter.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    public boolean isCompareFiles(MailLetter letter) {
        try {
            File file1 = new File(this.pathAttachFile +"\\" + letter.getSubject() + this.typeFile);
            File file2 = new File(this.pathDownloadFile + "\\"+ letter.getSubject() + this.typeFile);
            resultEqual = org.apache.commons.io.FileUtils.contentEquals(file1, file2);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return resultEqual;
    }

    public void waitForFile(MailLetter letter) {
        File fileWait = new File(this.pathDownloadFile + "\\"+ letter.getSubject() + this.typeFile);
        FluentWait<File> wait = new FluentWait<File>(fileWait).withTimeout(WAIT_FILE_DOWNLOAD_SECONDS, TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            @Override
            public Boolean apply(File fileWait) {
                com.epam.gomel.tat.ispiridonova.reporting.Logger.trace("Download File - " + fileWait.exists());
                return fileWait.exists();
            }
        });
    }
}

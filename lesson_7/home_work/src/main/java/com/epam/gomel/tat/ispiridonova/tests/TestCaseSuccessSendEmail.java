package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.service.LoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.LogoutGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseSuccessSendEmail {

    LoginGuiService loginGuiService = new LoginGuiService();
    LogoutGuiService logout = new LogoutGuiService();
    MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getMailLetter();

    @Test(description = "Login")
    public void login() {
        Logger.log.trace("TestCaseSuccessSendEmail");
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail", dependsOnMethods = "login")
    public void sendMail() {
        mailGuiService.sendMail(letter);
        Assert.assertTrue(mailGuiService.isMailInInbox(letter), "Email is not Inbox");
    }

    @Test(description = "logout", dependsOnMethods = "sendMail")
    public void logoutEmail() {
        logout.logout();
    }
}

package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailInboxListPage extends AbstractBasePage {

    private static final By COMPOSE_LINK_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By CHECK_MAIL_LOCATOR = By.xpath("//a[contains(@id,'nb-1')]");
    public static final String RECEIVE_EMAIL_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-messages-item')]//div[not(.//span[@class='b-messages__folder'])]//a[contains(@href, '#message')]";



    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_LINK_LOCATOR);
        return new ComposeMailPage();
    }

    public boolean checkLogin() {
        browser.waitForAjaxProcessed();
        return browser.isPresent(CHECK_MAIL_LOCATOR);
    }

    public MailInboxListPage openBoxMessage(String mailSubject) {
        browser.waitForAjaxProcessed();
        browser.openBoxEmail(mailSubject);
        return new MailInboxListPage();
    }

    public ReceiveMessagePage openReceiveMessage(String mailContent) {
        browser.waitForAjaxProcessed();
        browser.openReceiveEmail(mailContent);
        return new ReceiveMessagePage();
    }

    public MailInboxListPage markEmail(String mailContent) {
        browser.waitForAjaxProcessed();
        browser.markEmail(mailContent);
        return new MailInboxListPage();
    }

    public MailInboxListPage deleteEmail() {
        browser.deleteEmail();
        return new MailInboxListPage();
    }

    public MailInboxListPage spamEmail() {
        browser.spamEmail();
        return new MailInboxListPage();
    }

    public boolean isMessagePresent(String mailContent) {
        return browser.isPresent(By.xpath(String.format(RECEIVE_EMAIL_LOCATOR_PATTERN, mailContent)));
    }
}


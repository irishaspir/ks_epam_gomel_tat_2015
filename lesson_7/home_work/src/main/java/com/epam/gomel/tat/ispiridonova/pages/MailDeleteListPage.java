package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailDeleteListPage extends AbstractBasePage {

    public ReceiveMessagePage openDeleteMessage(String mailSubject) {
        browser.openEmailIn(mailSubject);
        browser.waitForAjaxProcessed();
        return new ReceiveMessagePage();
    }
}

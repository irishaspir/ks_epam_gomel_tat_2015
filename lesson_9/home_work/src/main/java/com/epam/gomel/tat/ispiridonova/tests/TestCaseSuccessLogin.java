package com.epam.gomel.tat.ispiridonova.tests;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.Credentials;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestCaseSuccessLogin {

    private MailLoginGuiService loginService = new MailLoginGuiService();
    private Account credentials = Credentials.RIGHT_CREDENTIALS.getCredentials();

    @Test(description = "Login")
    public void login() {
        loginService.loginToAccount(credentials);
    }

    @Test(description = "Check login",dependsOnMethods = "login")
    public void checkLogin(){
       Assert.assertTrue(loginService.isSuccessLogin(credentials), "Login is unsuccess");
    }

    @Test(description = "Logout",dependsOnMethods = "checkLogin")
    public void logoutEmail(){
        loginService.logout();
    }
}


package com.epam.gomel.tat.ispiridonova.service;

import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;

public class LogoutGuiService {
    public void logout(){
        Logger.log.trace("Logout ");
         new MailboxBasePage().logout();
    }
}

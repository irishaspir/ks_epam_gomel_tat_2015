package com.epam.gomel.tat.ispiridonova.utils;

import java.io.File;
import java.io.IOException;

public class FilesUtils {

    private File file;

    public String createFile(String pathFile, String mailSubject, String mailContent, String typeFile) {
        file = org.apache.commons.io.FileUtils.getFile(pathFile, mailSubject + typeFile);
        try {
            org.apache.commons.io.FileUtils.write(file, mailContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    public void compareFiles(String attachFile, String downloadFile) {
        try {
            File file1 = new File(attachFile);
            File file2 = new File(downloadFile);
            org.apache.commons.io.FileUtils.contentEquals(file1, file2);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

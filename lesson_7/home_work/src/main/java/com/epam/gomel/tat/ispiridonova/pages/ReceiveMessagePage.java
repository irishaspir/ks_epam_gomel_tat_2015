package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import org.openqa.selenium.By;

public class ReceiveMessagePage extends AbstractBasePage {

    public boolean isCheckReceiveMessage(MailLetter letter) {
        browser.isPresent(By.xpath(String.format(MAIL_SUBJECT_LOCATOR_PATTERN, letter.getSubject())));
        browser.isPresent(By.xpath(String.format(MAIL_CONTENT_LOCATOR_PATTERN, letter.getContent())));
        browser.isPresent(By.xpath(String.format(MAIL_ADRESS_LOCATOR_PATTERN, letter.getReceiver())));
        if (letter.getReceiver() != null) {

            browser.isPresent(By.xpath(String.format(MAIL_ATTACH_LOCATOR_PATTERN, letter.getReceiver())));
        }
        return true;
    }

    public void downloadFile(String mailSubject) {
        browser.downloadFile(mailSubject);
    }
}

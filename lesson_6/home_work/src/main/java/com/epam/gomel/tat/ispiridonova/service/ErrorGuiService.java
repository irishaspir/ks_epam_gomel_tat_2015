package com.epam.gomel.tat.ispiridonova.service;


import com.epam.gomel.tat.ispiridonova.bo.error.MessageMistake;
import com.epam.gomel.tat.ispiridonova.pages.ErrorLoginPage;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import org.testng.Assert;

public class ErrorGuiService {

    public boolean checkErrorMessage(MessageMistake message) {
        String errorLogin = new ErrorLoginPage().errorMessage();
        Logger.log.trace("Check that message " + errorLogin + " is equals " + message.getMessage());
        Assert.assertEquals(errorLogin, message.getMessage());
        return true;
    }
}

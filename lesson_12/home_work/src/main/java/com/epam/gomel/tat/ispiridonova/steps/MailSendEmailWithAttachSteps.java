package com.epam.gomel.tat.ispiridonova.steps;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailSendEmailWithAttachSteps extends Steps {

    protected MailLoginGuiService loginService = new MailLoginGuiService();
    private MailGuiService mailService = new MailGuiService();
    private Letter letter = LetterBuilder.randomValuesWithAttach();
    protected Account user;

    @BeforeStory
    public void beforeStory() {
        user = AccountBuilder.createUser();
        AccountBuilder.build(user).withCorrectLogin().withCorrectPassword().withEmail();
    }

    @Given("Actor has login, password and Actor login to mailbox")
    public void mailboxUser() {
        loginService.loginToAccount(user);
    }

    @When("Actor send email with attach")
    public void sendEmail() {
        mailService.sendEmailWithAttach(letter);
    }

    @Then("Actor see email with attach in inbox")
    public void isEmailInSendingList() {
        Assert.assertTrue(mailService.isEmailInInbox(letter), "Email isn't in Inbox");
    }
}

Feature: Mark email as spam

Meta:
@Mark email as spam

Narrative:
As a Mailbox user
I want to login in mailbox, send email and mark it as apam
So that I have access to mailbox list and  I see mail in spam list

Scenario: Mark email as spam
Given Actor login and Actor send email
When Actor mark email as spam
Then Actor see email in spam list
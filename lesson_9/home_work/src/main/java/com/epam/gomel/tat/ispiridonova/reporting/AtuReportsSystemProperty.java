package com.epam.gomel.tat.ispiridonova.reporting;

import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import com.epam.gomel.tat.ispiridonova.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

@Listeners({ATUReportsListener.class, ConfigurationListener.class,
        MethodListener.class})

public class AtuReportsSystemProperty {

    private static final String ATU_PROPERTIES_PATH = Utils.getCanonicalPathToResourceFile("/atu.properties");

    {
        System.setProperty("atu.reporter.config", ATU_PROPERTIES_PATH);
    }

    @BeforeClass
    public void init() {
        setIndexPageDescription();
        setAuthorInfoForReports();
    }

    private void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Automation Tester", atu.testng.reports.utils.Utils.getCurrentTime(), "1.0");
    }

    private void setIndexPageDescription() {
        ATUReports.indexPageDescription = "My Project Description <br/> <b>Can include Full set of HTML Tags</b>";
    }
}

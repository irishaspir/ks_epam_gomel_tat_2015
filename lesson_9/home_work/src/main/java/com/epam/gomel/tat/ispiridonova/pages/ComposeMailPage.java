package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

import java.io.File;

public class ComposeMailPage{

    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By CONTENT_TEXTAREA_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By ATTACH_FILE_INPUT_LOCATOR = By.xpath("//input[@name='att']");

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        return sendMail(mailTo, mailSubject, mailContent, null);
    }

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent, File attachFile) {
       Browser.get().type(TO_INPUT_LOCATOR, mailTo);
       Browser.get().type(SUBJECT_INPUT_LOCATOR, mailSubject);
       Browser.get().type(CONTENT_TEXTAREA_LOCATOR, mailContent);
        if (attachFile != null) {

           Browser.get().attachFile(ATTACH_FILE_INPUT_LOCATOR, attachFile.getAbsolutePath());
        }
       Browser.get().click(SEND_MAIL_BUTTON_LOCATOR);
       Browser.get().waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}

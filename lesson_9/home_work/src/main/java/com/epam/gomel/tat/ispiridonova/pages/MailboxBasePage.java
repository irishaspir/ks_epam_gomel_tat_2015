package com.epam.gomel.tat.ispiridonova.pages;

import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.openqa.selenium.By;

public class MailboxBasePage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//div[@class='b-folders__i']//a[@href='#inbox']");
    private static final By INBOX_PAGE_LOCATOR = By.xpath("//span[@class='js-messages-title-dropdown-name']");
    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    private static final By SPAM_PAGE_LOCATOR = By.xpath("//div[@class='b-messages-head']//label[@class ='b-messages-head__title']/input[@class = 'b-messages-head__checkbox']");
    private static final By USER_NAME_LOCATOR = By.xpath("//a[contains(@id,'nb-1')]");
    private static final By DISK_LINK_LOCATOR = By.xpath("//a[contains(@href,'disk.yandex.by')]");
    private static final By LOGOUT_LOCATOR = By.xpath("//div[not(.//a[contains(@data-action,'exitAll')])]//a[contains(@href,'logout')]");

    public MailInboxListPage openInboxPage() {
        Browser.get().waitForVisible(INBOX_LINK_LOCATOR);
        Browser.get().click(INBOX_LINK_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        Browser.get().waitForVisible(INBOX_PAGE_LOCATOR);
        return new MailInboxListPage();
    }

    public MailDeleteListPage openDeletePage() {
        Browser.get().click(DELETE_LINK_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        return new MailDeleteListPage();
    }

    public MailSpamListPage openSpamPage() {
        Browser.get().waitForVisible(SPAM_LINK_LOCATOR);
        Browser.get().click(SPAM_LINK_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        Browser.get().waitForVisible(SPAM_PAGE_LOCATOR);
        return new MailSpamListPage();
    }

    public void logout() {
        Browser.get().click(USER_NAME_LOCATOR);
        Browser.get().click(LOGOUT_LOCATOR);
        Browser.get().waitForAjaxProcessed();
    }

    public String getUserEmail() {
        return Browser.get().getText(USER_NAME_LOCATOR);
    }

    public boolean isCheckLogin() {
        Logger.trace("Check success login " + USER_NAME_LOCATOR);
        return Browser.get().isPresent(USER_NAME_LOCATOR);
    }

    public DiskPage goToDisk() {
        Browser.get().click(DISK_LINK_LOCATOR);
        Browser.get().waitForAjaxProcessed();
        return new DiskPage();
    }
}

package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TestCaseMarkEmailAsNotSpam {

    private String userLogin = "test-box-IS"; // ACCOUNT
    private String userPassword = "Qwert123"; // ACCOUNT
    private String mailTo = "test-box-IS@yandex.ru"; // ENUM
    private String mailSubject;
    private String mailContent;
    private String attachFile = null;
    private int fileNameLetter = 10;
    private int contentLetter = 20;

    @Test(description = "Login")
    public void login() {
        new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
    }

    @Test(description = "Send email", dependsOnMethods = "login")
    public void sendEmail() {
        RandomUtils generateRandom = new RandomUtils();
        mailSubject = generateRandom.subject(fileNameLetter);
        mailContent = generateRandom.content(contentLetter);
        new MailboxBasePage()
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent, attachFile);
    }

    @Test(description = "Spam Letter", dependsOnMethods = "sendEmail")
    public void spamEmail() {
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(mailSubject)
                .markEmail(mailContent)
                .spamEmail();
        boolean checkMessage = new MailboxBasePage()
                .openSpamPage()
                .isMessagePresent(mailSubject);
        Assert.assertTrue(checkMessage, "Email is absent");
    }

    @Test(description = "Not spam email", dependsOnMethods = "spamEmail")
    public void notSpamEmail() {
        new MailboxBasePage()
                .openSpamPage()
                .markEmail(mailSubject)
                .notSpamEmail();
        boolean checkMessage = new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(mailSubject)
                .isMessagePresent(mailContent);
        Assert.assertTrue(checkMessage, "Email is absent");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}

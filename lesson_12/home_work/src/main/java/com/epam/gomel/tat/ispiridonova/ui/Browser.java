package com.epam.gomel.tat.ispiridonova.ui;

import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Browser {

    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 180;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 40;
    private static final int AJAX_TIMEOUT = 40;
    private static final int WAIT_ELEMENT_TIMEOUT = 60;
    private static final int TIME_OUT_IN_SECONDS = 40;

    private static final String FILE_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, text/csv, application/octet-stream, application/txt, image/jpeg";
    private static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private boolean augmented;

    private static WebDriver driver;
    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();

    }

    private static Browser init() {
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
      //  driver.manage().window().maximize();
        Logger.log.trace("Open Firefox: pageLoadTimeout" + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + " implicitlyWait " + COMMAND_DEFAULT_TIMEOUT_SECONDS);
        return new Browser(driver);

    }

    private static FirefoxProfile getFireFoxProfile() {
        Logger.trace("Download dir " + GlobalConfig.getDownloadDirFirefox() + " Type files to save " + FILE_TYPES_TO_SAVE);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalConfig.getDownloadDirFirefox());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FILE_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String url) {
        Logger.trace("Open url " + url);
        driver.get(url);
    }

    public static void killBrowser() {
        Logger.trace("Close Browser");
        driver.quit();
        instance = null;
    }

    public void click(By locator) {
        Logger.trace("Click on element " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.click();
        takeScreenshot();
    }

    public void type(By locator, String text) {
        Logger.trace("Type text " + text + " in " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.clear();
        element.sendKeys(text);
        takeScreenshot();
    }

    public void attachFile(By locator, String text) {
        Logger.trace("Type to " + locator + "  path of file " + text);
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isPresent(By locator) {
        Logger.trace("Find element " + locator);
        return driver.findElements(locator).size() > 0;
    }

    public String getText(By locator) {
        Logger.trace("Taken message " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator).getText();
    }

    public void waitElementIsPresent(final By locator) {
        Logger.trace("Wait " + TIME_OUT_IN_SECONDS + " element " + locator + " is present in Inbox");
        new WebDriverWait(driver, TIME_OUT_IN_SECONDS).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.trace("Wait " + WAIT_ELEMENT_TIMEOUT + " element " + locator + " is visible");
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForInvisible(By locator) {
        Logger.trace("Wait " + WAIT_ELEMENT_TIMEOUT + " element " + locator + " is invisible");
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void submit(By locator) {
        Logger.trace("Click on " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.submit();
        takeScreenshot();
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

    public void takeScreenshot() {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }
}

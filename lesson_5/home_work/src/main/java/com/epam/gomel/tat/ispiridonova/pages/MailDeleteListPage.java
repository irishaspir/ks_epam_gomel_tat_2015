package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class MailDeleteListPage extends AbstractBasePage {

    public static final String DELETE_EMAIL_CHECK_LOCATOR_PATTERN = "//span[@title='%s']";

    public boolean isMessagePresent(String subject) {
        return browser.isPresent(By.xpath(String.format(DELETE_EMAIL_CHECK_LOCATOR_PATTERN, subject)));
    }
}

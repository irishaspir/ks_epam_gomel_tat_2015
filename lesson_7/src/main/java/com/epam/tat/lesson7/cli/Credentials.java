package com.epam.tat.lesson7.cli;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Credentials {

    private String username;

    private String password;

    public Credentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

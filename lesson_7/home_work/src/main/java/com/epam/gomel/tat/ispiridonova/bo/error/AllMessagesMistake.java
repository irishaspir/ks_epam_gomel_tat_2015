package com.epam.gomel.tat.ispiridonova.bo.error;

/**
 * Created by Ira on 26.07.2015.
 */
public enum AllMessagesMistake {

    LOGIN_ERROR("Неправильный логин или пароль.");

    private MessageMistake message;

    AllMessagesMistake(String error) {
        message = new MessageMistake(error);
    }

    public MessageMistake getMessageMistake(){
        return  message;
    }
}

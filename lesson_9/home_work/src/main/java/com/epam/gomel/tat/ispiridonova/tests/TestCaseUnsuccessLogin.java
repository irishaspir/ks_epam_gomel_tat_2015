package com.epam.gomel.tat.ispiridonova.tests;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.Credentials;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseUnsuccessLogin {

    private MailLoginGuiService loginService = new MailLoginGuiService();
    private Account credentials = Credentials.WRONG_CREDENTIALS.getCredentials();

    @Test(description = "Login ")
    public void login() {
        loginService.loginToAccount(credentials);
        Assert.assertFalse(loginService.isSuccessLogin(credentials), "Login is success");
    }

    @Test(description = "Check error login message ", dependsOnMethods = "login")
    public void checkErrorMessage(){
      Assert.assertTrue(loginService.isErrorMessagesRight()," Error message isn't right");
    }
}






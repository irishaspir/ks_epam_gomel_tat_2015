package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.Credentials;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseSuccessSendEmailWithAttachment {

    private MailLoginGuiService loginService = new MailLoginGuiService();
    private MailGuiService mailService = new MailGuiService();
    private Account credentials = Credentials.RIGHT_CREDENTIALS.getCredentials();
    private Letter letter = LetterBuilder.randomValuesWithAttach();

    @Test(description = "Login")
    public void login() {
        loginService.loginToAccount(credentials);
        Assert.assertTrue(loginService.isSuccessLogin(credentials), "Login is unsuccess");
    }

    @Test(description = "Send email", dependsOnMethods = "login")
    public void sendEmail() {
        mailService.sendEmailWithAttach(letter);
        Assert.assertTrue(mailService.isEmailInInbox(letter), "Email isn in Inbox");
    }

    @Test(description = "Download file from receive email", dependsOnMethods = "sendEmail")
    public void downloadFile() {
        mailService.downloadFile(letter);
    }

    @Test(description = "Compare download and attach files ", dependsOnMethods = "downloadFile")
    public void compareDownloadAndAttachFiles() {
        Assert.assertTrue(mailService.isFilesEqual(letter.getAttach()), "Files aren't equal");
    }

    @Test(description = "Logout", dependsOnMethods = "compareDownloadAndAttachFiles")
    public void logoutEmail() {
        loginService.logout();
    }

}

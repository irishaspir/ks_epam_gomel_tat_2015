package com.epam.gomel.tat.ispiridonova.bo.common;

public enum Credentials {

    RIGHT_CREDENTIALS("test-box-IS", "Qwert123", "test-box-IS@yandex.ru"),
    WRONG_CREDENTIALS("test-box-IS", "Qwert12", "test-box-IS@yandex.ru");

    private Account account;

    Credentials(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getCredentials() {
        return account;
    }
}

package com.epam.gomel.tat.ispiridonova.runner;


import com.epam.gomel.tat.ispiridonova.reporting.CustomTestNgListener;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        testNG.addListener(new CustomTestNgListener());
        for (String arg : args) {
            Logger.info("Run suit:" + arg);
            suites.add(arg);
        }
        testNG.setTestSuites(suites);
        testNG.run();
    }
}

package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.pages.MailLoginPage;
import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import com.epam.gomel.tat.ispiridonova.utils.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TestCaseDeleteEmail {

    private String userLogin = "test-box-IS";
    private String userPassword = "Qwert123";
    private String mailTo = "test-box-IS@yandex.ru";
    private String mailSubject;
    private String mailContent;
    private String attachFile = null;
    private int fileNameLetter = 10; //
    private int contentLetter = 20;

    @Test(description = "Login")
    public void login() {
        new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
    }

    @Test(description = "Send email", dependsOnMethods = "login")
    public void sendEmail() {
        RandomUtils generateRandom = new RandomUtils();
        mailSubject = generateRandom.subject(fileNameLetter);
        mailContent = generateRandom.content(contentLetter);
        new MailboxBasePage()
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent, attachFile);
    }

    @Test(description = "Check email", dependsOnMethods = "sendEmail")
    public void checkEmail() {
        boolean checkMessage = new MailboxBasePage()
                .openSendPage()
                .isMessagePresent(mailSubject);
        Assert.assertTrue(checkMessage, "Message is absent");
    }

    @Test(description = "Delete email", dependsOnMethods = "checkEmail")
    public void deleteEmail() {
        new MailboxBasePage()
                .openInboxPage()
                .openBoxMessage(mailSubject)
                .markEmail(mailContent)
                .deleteEmail();
        boolean checkMessage = new MailboxBasePage()
                .openDeletePage()
                .isMessagePresent(mailSubject);
        Assert.assertTrue(checkMessage, "Email is absent");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}

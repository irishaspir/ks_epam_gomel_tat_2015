package com.epam.gomel.tat.ispiridonova.ui;

public enum ParallelMode {

    FALSE("false"),
    TESTS("tests"),
    CLASSES("classes"),
    METHODS("methods"),
    SUITES("suits");

    private String type;

    private ParallelMode(String type) {
        this.type = type;
    }

    public static ParallelMode getTypeByAlias(String alias) {
        for(ParallelMode type: ParallelMode.values()){
            if(type.getType().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getType() {
        return type;
    }
}

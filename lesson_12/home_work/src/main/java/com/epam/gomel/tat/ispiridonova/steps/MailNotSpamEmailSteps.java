package com.epam.gomel.tat.ispiridonova.steps;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailNotSpamEmailSteps extends Steps {

    protected MailLoginGuiService loginService = new MailLoginGuiService();
    private MailGuiService mailService = new MailGuiService();
    private Letter letter = LetterBuilder.randomValues();
    protected Account user;

    @BeforeStory
    public void beforeStory() {
        user = AccountBuilder.createUser();
        AccountBuilder.build(user).withCorrectLogin().withCorrectPassword().withEmail();
    }

    @Given("Actor is in inbox and Actor send email")
    public void actorSendEmail() {
        loginService.loginToAccount(user);
        mailService.sendEmail(letter);
    }

    @When("Actor mark email as spam and return it in inbox")
    public void removeEmailInSpamAndReturn() {
        mailService.removeEmailToSpam(letter);
        Assert.assertTrue(mailService.isEmailInSpam(letter), "Email isn't in Spam");
        mailService.removeEmailToInbox(letter);
    }

    @Then("Actor see returned email in inbox")
    public void isEmailInInbox() {
        Assert.assertTrue(mailService.isEmailInInbox(letter), "Email isn't in inbox");
    }
}

package com.epam.gomel.tat.ispiridonova.bo.common;

public enum DefaultAccounts {

    DEFAULT_MAIL("test-box-IS", "Qwert123", "test-box-IS@yandex.ru"),
    UNSUCCESS_MAIL("test-box-IS", "Qwert12", "test-box-IS@yandex.ru");

    private Account account;

    DefaultAccounts(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getAccount() {
        return account;
    }
}

package com.epam.gomel.tat.ispiridonova.steps;

import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import com.epam.gomel.tat.ispiridonova.ui.Browser;
import org.jbehave.core.annotations.*;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailSendEmailSteps extends Steps {

    protected MailLoginGuiService loginService = new MailLoginGuiService();
    private MailGuiService mailService = new MailGuiService();
    private Letter letter = LetterBuilder.randomValues();
    protected Account user;

    @BeforeStory
    public void beforeStory() {
        user = AccountBuilder.createUser();
        AccountBuilder.build(user).withCorrectLogin().withCorrectPassword().withEmail();
    }

    @Given("Actor has correct login, correct password and Actor login to mailbox")
    public void actorLogin() {
        loginService.loginToAccount(user);
    }

    @When("Actor send email")
    public void actorSendEmail() {
        mailService.sendEmail(letter);
    }

    @Then("Actor see email in inbox")
    public void isEmailInSendingList() {
        Assert.assertTrue(mailService.isEmailInInbox(letter), "Email isn't in Inbox");
    }

    @AfterStory
    public void afterStory() {
        Browser.killBrowser();
    }
}

package com.epam.gomel.tat.ispiridonova;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test_Case_Mark_email_as_a_spam {

    // AUT data
    public static final String BASE_URL = "http://www.yandex.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[descendant::span[contains(@class, 'button__text')]]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@data-action='tospam']");
    public static final By SPAM_LETTERS_BOX_LOCATOR = By.xpath( "//a[@href='#spam']");
    public static final String SPAM_LETTER_CHECK_LOCATOR_PATTERN = "//span[@title='%s']";
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[.//*[text()='%s']]";
    public static final String MARK_LETTER_LOCATOR_PATTERN = "//*[text()='%s']/ancestor::div[contains(@class, 'block-thread')]//div[not(.//span[@class='b-messages__folder'])]//input[@class='b-messages__message__checkbox__input']";


    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 40;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 60;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    public static final int DOWNLOAD_WAIT_TIMEOUT_SECOND = 20;
    private WebDriver driver;

    // Test data
    private String userLogin = "test-box-IS"; // ACCOUNT
    private String userPassword = "Qwert123"; // ACCOUNT
    private String mailTo = "test-box-IS@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM



    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {

        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }



    @BeforeMethod(description = "Login and send new letter")
    public void  precondition()
    {
        //login in mail
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        //send letter
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        WebElement inboxLink = driver.findElement(INPOX_LINK_LOCATOR);
        inboxLink.click();

        //wait letter and check letter
        new WebDriverWait(driver,TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));
        WebElement receiveLetter = driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        Assert.assertTrue(receiveLetter.isEnabled(), "Letter isn't recieved");

    }

    @Test(description = "Delete Letter")
    public void login() {

        //open box whit letters
        WebElement openBoxWithLetters = driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        openBoxWithLetters.click();

        //mark letter
        WebElement markLetter = driver.findElement(By.xpath(String.format(MARK_LETTER_LOCATOR_PATTERN, mailContent)));
        markLetter.click();

        //remove letter to spam
        WebElement removeToSpam = driver.findElement(SPAM_LINK_LOCATOR);
        removeToSpam.click();
        new WebDriverWait(driver, DOWNLOAD_WAIT_TIMEOUT_SECOND).until(ExpectedConditions.visibilityOfElementLocated(SPAM_LETTERS_BOX_LOCATOR));

        // come in spam
        WebElement spamMessageBox = driver.findElement(SPAM_LETTERS_BOX_LOCATOR);
        spamMessageBox.click();
        new WebDriverWait(driver, DOWNLOAD_WAIT_TIMEOUT_SECOND).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(SPAM_LETTER_CHECK_LOCATOR_PATTERN, mailSubject))));


        // check that letter is in box
        WebElement checkSpamLetter = driver.findElement(By.xpath(String.format(SPAM_LETTER_CHECK_LOCATOR_PATTERN, mailSubject )));
        Assert.assertTrue(checkSpamLetter.isEnabled(), "Letter is absent ");

    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

}

package com.epam.gomel.tat.ispiridonova.ui;


import com.epam.gomel.tat.ispiridonova.cli.TestRunnerOptions;

public class GlobalConfig {

    private static final String BASE_URL = "http://www.mail.yandex.ru";

    private static String downloadDir;

    private static BrowserType browserType = BrowserType.FIREFOX;

    private static ParallelMode parallelMode = ParallelMode.FALSE;

    private static String localHost = "localhost";

    private static String port = "4444";

    private static int threadCount = 1;

    private static String typeFile = ".txt";

    private static int length = 10;

    private static final int WAIT_FILE_DOWNLOAD_SECONDS = 20;

    public static void updateFromOptions(TestRunnerOptions options) {
        browserType = BrowserType.getTypeByAlias(options.browserType);
        parallelMode = ParallelMode.getTypeByAlias(options.parallelMode);
        threadCount = options.threadCount;
        localHost = options.localHost;
        port = options.port;
    }

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static void setBrowserType(BrowserType browserType) {
        GlobalConfig.browserType = browserType;
    }

    public static ParallelMode getParallelMode() {
        return parallelMode;
    }

    public static void setParallelMode(ParallelMode parallelMode) {
        GlobalConfig.parallelMode = parallelMode;
    }

    public static int getThreadCount() {
        return threadCount;
    }

    public static void setThreadCount(int threadCount) {
        GlobalConfig.threadCount = threadCount;
    }

    public static String getLocalHost() {
        return localHost;
    }

    public static String getPort() {
        return port;
    }

    public static String getDownloadDir() {
        return downloadDir;
    }

    public static void setDownloadDir(String downloadDir) {
        GlobalConfig.downloadDir = downloadDir;
    }

    public static String getTypeFile() {
        return typeFile;
    }

    public static String getBASEURL() {
        return BASE_URL;
    }

    public static int getLenght() {
        return length;
    }

    public static void setLenght(int length) { GlobalConfig.length = length; }

    public static int getWaitFileDownloadSeconds(){return WAIT_FILE_DOWNLOAD_SECONDS;}
}

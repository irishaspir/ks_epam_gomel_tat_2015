package com.epam.gomel.tat.ispiridonova.pages;

import org.openqa.selenium.By;

public class ErrorLoginPage extends AbstractBasePage {

    public static final By CHECK_MESSAGE_MISTAKE_LOCATOR = By.xpath("//div[contains(@class, 'js-messages')]//div[contains(@class, 'error-msg')]");

    public String errorMessage() {
        return browser.errorMessage(CHECK_MESSAGE_MISTAKE_LOCATOR);
    }
}

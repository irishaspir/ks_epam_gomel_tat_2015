package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.Credentials;
import com.epam.gomel.tat.ispiridonova.bo.mail.Letter;
import com.epam.gomel.tat.ispiridonova.bo.mail.LetterBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.AtuReportsSystemProperty;
import com.epam.gomel.tat.ispiridonova.service.MailLoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.DiskGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseWorkWithDisk extends AtuReportsSystemProperty {

    private MailLoginGuiService loginService = new MailLoginGuiService();
    private DiskGuiService diskService = new DiskGuiService();
    private Account credentials = Credentials.RIGHT_CREDENTIALS.getCredentials();
    private Letter letter = LetterBuilder.randomValuesWithAttach();
    private String fileName;

    @Test(description = "Login")
    public void login() {
        loginService.loginToAccount(credentials);
        Assert.assertTrue(loginService.isSuccessLogin(credentials), "Login is unsuccess");
    }

    @Test(description = "Go to Disk", dependsOnMethods = "login")
    public void goToDisk() {
        diskService.goToDisk();
    }

    @Test(description = "Load file", dependsOnMethods = "goToDisk")
    public void loadFile() {
        fileName = diskService.loadFile(letter.getAttach());
    }

    @Test(description = "Download file", dependsOnMethods = "loadFile")
    public void downloadFile() {
        diskService.downloadFile(fileName);
    }

    @Test(description = "Remove file in trash", dependsOnMethods = "downloadFile")
    public void removeFileInBasket() {
        diskService.removeFileInTrash(fileName);
    }

    @Test(description = "Open trash and restore file", dependsOnMethods = "removeFileInBasket")
    public void openTrashAndRestoreFile() {
        diskService.openTrashAndRestoreFile(fileName);
        Assert.assertTrue(diskService.isFileRestored(fileName), "File isn't restored");
    }

    @Test(description = "Logout", dependsOnMethods = "openTrashAndRestoreFile")
    public void logoutEmail() {
        loginService.logoutFromDisk();
    }
}

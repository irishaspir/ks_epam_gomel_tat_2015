package com.epam.gomel.tat.ispiridonova.utils;


import org.apache.commons.lang3.RandomStringUtils;

public  class RandomUtils {

    private String mailSubject = "test subject";
    private String mailContent = "mail content";

    public String subject(int x){
        return mailSubject + RandomStringUtils.randomAlphabetic(x);
    }

    public String content(int x){
        return mailContent + RandomStringUtils.randomAlphanumeric(x);
    }
}

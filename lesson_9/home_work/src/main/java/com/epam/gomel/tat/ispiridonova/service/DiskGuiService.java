package com.epam.gomel.tat.ispiridonova.service;

import com.epam.gomel.tat.ispiridonova.pages.MailboxBasePage;
import com.epam.gomel.tat.ispiridonova.pages.DiskTrashPage;
import com.epam.gomel.tat.ispiridonova.pages.DiskPage;
import com.epam.gomel.tat.ispiridonova.reporting.AtuLogger;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.utils.Utils;

import java.io.File;

public class DiskGuiService {

    public void goToDisk() {
        AtuLogger.info("Go to  Disk ");
        Logger.log.info("Go to Disk ");
        new MailboxBasePage()
                .goToDisk()
                .isDisk();
    }

    public String loadFile(File file) {
        new DiskPage().loadFile(file);
        return file.getName();
    }

    public void downloadFile(String fileName) {
        Logger.log.info("Download file ");
        AtuLogger.info("Download file ");
        new DiskPage()
                .chooseFile(fileName)
                .downloadFile(fileName);
        new Utils().waitForFileOnDisk(fileName);
    }

    public void removeFileInTrash(String fileName) {
        Logger.log.info("Remove file to trash ");
        AtuLogger.info("Remove file to trash ");
        new DiskPage()
                .chooseFile(fileName)
                .removeFileToTrash(fileName);
    }

    public void openTrashAndRestoreFile(String fileName) {
        Logger.log.info("Open trash and restore file");
        AtuLogger.info("Open trash and restore file");
        new DiskPage()
                .openTrash()
                .chooseFileAndRestore(fileName);
    }

    public boolean isFileRestored(String fileName) {
        new DiskTrashPage()
                .goToFiles()
                .chooseFile(fileName);
        return true;
    }
}

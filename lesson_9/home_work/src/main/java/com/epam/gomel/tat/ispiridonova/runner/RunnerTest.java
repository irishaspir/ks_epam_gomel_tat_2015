package com.epam.gomel.tat.ispiridonova.runner;

import com.epam.gomel.tat.ispiridonova.reporting.CustomTestNgListener;
import com.epam.gomel.tat.ispiridonova.ui.GlobalConfig;
import com.epam.gomel.tat.ispiridonova.cli.TestRunnerOptions;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public class RunnerTest {
    TestRunnerOptions options = new TestRunnerOptions();
    private Collection<XmlSuite> suitList;
    private Collection<XmlSuite> allSuits;

    private RunnerTest(String[] args) {
        parserCli(args);
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        new RunnerTest(args).runTests();
    }

    private void parserCli(String[] args) {
        CmdLineParser parser = new CmdLineParser(options);
        try {
            parser.parseArgument(args);
            GlobalConfig.updateFromOptions(options);
        } catch (CmdLineException e) {
            System.err.println();
        }
        System.out.println(options);
    }

    private void runTests() throws IOException, SAXException, ParserConfigurationException {
        int a;
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());

        allSuits = new Parser(options.suits.get(0)).parse();
        ((List<XmlSuite>) allSuits).get(0).setParallel(GlobalConfig.getParallelMode().getType());
        ((List<XmlSuite>) allSuits).get(0).setThreadCount(GlobalConfig.getThreadCount());

        for (a = 1; a < options.suits.size(); a++) {
            suitList = new Parser(options.suits.get(a)).parse();
               ((List<XmlSuite>) suitList).get(0).setParallel(GlobalConfig.getParallelMode().getType());
               ((List<XmlSuite>) suitList).get(0).setThreadCount(GlobalConfig.getThreadCount());
            for (XmlSuite i: suitList){
                allSuits.add(i);
            System.out.println(i);}
        }
        testNG.setXmlSuites((List<XmlSuite>) allSuits);
        testNG.run();
    }
}

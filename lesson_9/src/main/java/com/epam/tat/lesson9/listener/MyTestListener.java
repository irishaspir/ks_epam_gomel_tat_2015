package com.epam.tat.lesson9.listener;

import com.epam.tat.lesson9.utils.Logger;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

/**
 * Created by Aleh_Vasilyeu on 4/1/2015.
 */
public class MyTestListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        Logger.info("method started: " + method.getTestMethod().getMethodName());

    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        Logger.info("method finished [" + testResult.getStatus() + "]: " + method.getTestMethod().getMethodName());
    }
}

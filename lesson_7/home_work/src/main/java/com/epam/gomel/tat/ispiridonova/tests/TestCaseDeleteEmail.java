package com.epam.gomel.tat.ispiridonova.tests;


import com.epam.gomel.tat.ispiridonova.bo.common.Account;
import com.epam.gomel.tat.ispiridonova.bo.common.AccountBuilder;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetter;
import com.epam.gomel.tat.ispiridonova.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.ispiridonova.reporting.Logger;
import com.epam.gomel.tat.ispiridonova.service.LoginGuiService;
import com.epam.gomel.tat.ispiridonova.service.LogoutGuiService;
import com.epam.gomel.tat.ispiridonova.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCaseDeleteEmail {

    LoginGuiService loginEmail = new LoginGuiService();
    LogoutGuiService logout = new LogoutGuiService();
    MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getMailLetter();

    @Test(description = "login")
    public void loginSuccess() {
        Logger.log.trace("TestCaseDeleteEmail");
        loginEmail.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail", dependsOnMethods = "loginSuccess")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check email in Inbox", dependsOnMethods = "sendMail")
    public void checkEmailInInbox() {
        Assert.assertTrue(mailGuiService.isMailInInbox(letter), "Email is not Inbox");
    }

    @Test(description = "Delete email", dependsOnMethods = "checkEmailInInbox")
    public void deleteEmail() {
        mailGuiService.deleteMail(letter);
    }

    @Test(description = "Check email in Delete List", dependsOnMethods = "deleteEmail")
    public void checkEmailInDeleteList() {
        Assert.assertTrue(mailGuiService.isMailInDelete(letter), "Email is not Delete List");
    }

    @Test(description = "logout", dependsOnMethods = "checkEmailInDeleteList")
    public void logoutEmail() {
        logout.logout();
    }

}

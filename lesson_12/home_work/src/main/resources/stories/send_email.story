Feature: Send email

Meta:
$Send email

Narrative:
As a Mailbox user
I want to login in mailbox and send email
So that I have access to mailbox list and see it in inbox

Scenario: Send email
Given Actor has correct login, correct password and Actor login to mailbox
When Actor send email
Then Actor see email in inbox